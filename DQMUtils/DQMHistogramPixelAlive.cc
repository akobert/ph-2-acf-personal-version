/*!
        \file                DQMHistogramPixelAlive.h
        \brief               base class to create and fill monitoring histograms
        \author              Adam Kobert
        \version             1.0
        \date                4/10/23
        Support :            mail to : 
 */

#include "DQMUtils/DQMHistogramPixelAlive.h"
#include "HWDescription/ReadoutChip.h"
#include "RootUtils/HistContainer.h"
#include "RootUtils/RootContainerFactory.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "Utils/ChannelContainerStream.h"
#include "Utils/Container.h"
#include "Utils/ContainerFactory.h"
#include "Utils/ContainerStream.h"
#include "Utils/EmptyContainer.h"
#include "Utils/HybridContainerStream.h"
#include "Utils/Occupancy.h"
#include "Utils/ThresholdAndNoise.h"
#include "Utils/Utilities.h"

using namespace Ph2_HwDescription;

//========================================================================================================================
DQMHistogramPixelAlive::DQMHistogramPixelAlive() {}

//========================================================================================================================
DQMHistogramPixelAlive::~DQMHistogramPixelAlive() {}
//========================================================================================================================
void DQMHistogramPixelAlive::book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_Parser::SettingsMap& pSettingsMap)
{
    // copy detector structrure
    fDetectorContainer = &theDetectorStructure;

    // find front-end types
    for(auto cBoard: *fDetectorContainer)
    {
        auto cFrontEndTypes = cBoard->connectedFrontEndTypes();
        fWithCBC            = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::CBC3) != cFrontEndTypes.end();
        fWithSSA            = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA) != cFrontEndTypes.end() ||
                   std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA2) != cFrontEndTypes.end();
        fWithMPA = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::MPA) != cFrontEndTypes.end() ||
                   std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::MPA2) != cFrontEndTypes.end();
    }

    std::vector<FrontEndType> cPixelTypes             = {FrontEndType::MPA, FrontEndType::MPA2, FrontEndType::SSA, FrontEndType::SSA2};
    
    auto selectPixelChipFunction = [cPixelTypes](const ChipContainer* pChip) {
        return (std::find(cPixelTypes.begin(), cPixelTypes.end(), static_cast<const ReadoutChip*>(pChip)->getFrontEndType()) != cPixelTypes.end());
    };


/*
    // find maximum number of channels
    std::vector<size_t> cNPixelChannels(0), cNStripChannels(0);
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
//                    auto cNChannels = theDetectorStructure.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->size();
                    auto cType      = cChip->getFrontEndType();

		    uint32_t NCH = NCHANNELS;
                    if(cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                        NCH = NSSACHANNELS;
                    else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
                        NCH = NMPACHANNELS;

//                    if(cType == FrontEndType::SSA || cType == FrontEndType::SSA2) { cNPixelChannels.push_back(cNChannels); }
//                    else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
//                    {
//                        cNPixelChannels.push_back(cNChannels);
//                    }
		    cNPixelChannels.push_back(NCH);

                }
            }
        }
    }
*/
    // find maximum number of channels
    std::vector<size_t> cNPixelChannels(0), cNStripChannels(0);
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    auto cNChannels = theDetectorStructure.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->size();
                    auto cType      = cChip->getFrontEndType();
                    if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2) { cNStripChannels.push_back(cNChannels); }
                    else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
                    {
                        cNPixelChannels.push_back(cNChannels);
                    }
                }
            }
        }
    }
    if(fWithMPA || fWithSSA) { fNPixelChannels = *std::max_element(std::begin(cNPixelChannels), std::end(cNPixelChannels)); }


    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);


    if(fWithMPA || fWithSSA)
    {
        // Set query function to only include strip chips in the data container
        fDetectorContainer->setReadoutChipQueryFunction(selectPixelChipFunction);


        // Validation
        HistContainer<TH1F> theTH1FPixelValidationContainer("Occupancy", "Occupancy", fNPixelChannels, -0.5, float(fNPixelChannels) - 0.5);
        RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorPixelValidationHistograms, theTH1FPixelValidationContainer);
	LOG(INFO) << BOLDYELLOW << "Save PixelAlive Histograms" << RESET;
        // Reset query function from only including strip chips in the data container
        fDetectorContainer->resetReadoutChipQueryFunction();
    }

}
bool DQMHistogramPixelAlive::fill(std::vector<char>& dataBuffer)
{
    HybridContainerStream<Occupancy, Occupancy, Occupancy> theOccupancy("PixelAlive");

    if(theOccupancy.attachBuffer(&dataBuffer))
    {
        LOG(INFO) << BOLDYELLOW << "Fill PixelAlive Histogram" << RESET;
        theOccupancy.decodeData(fDetectorData);
        fillValidationPlots(fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }
/*
    else if(theSCurve.attachBuffer(&dataBuffer))
    {
        std::cout << "Matched PedeNoise SCurve!!!!!\n";
        theSCurve.decodeChipData(fDetectorData);
        fillSCurvePlots(theSCurve.getHeaderElement<0>(), theSCurve.getHeaderElement<1>(), fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }
    else if(theThresholdAndNoiseStream.attachBuffer(&dataBuffer))
    {
        std::cout << "Matched PedeNoise ThresholdAndNoise!!!!!\n";
        theThresholdAndNoiseStream.decodeChipData(fDetectorData);
        fillPedestalAndNoisePlots(fDetectorData);

        fDetectorData.cleanDataStored();
        return true;
    }
*/

    return false;
}

//========================================================================================================================
void DQMHistogramPixelAlive::process()
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                std::string validationCanvasName = "PixelAlive_Validation_B_" + std::to_string(cBoard->getId()) + "_O_" + std::to_string(cOpticalGroup->getId()) + "_H_" + std::to_string(cHybrid->getId());
		bool fPlotSCurves = false;
                TCanvas* cValidation = new TCanvas(validationCanvasName.data(), validationCanvasName.data(), 0, 0, 650, fPlotSCurves ? 900 : 650);

                cValidation->Divide(cHybrid->size());

                for(auto cChip: *cHybrid)
                {
                    auto cType = cChip->getFrontEndType();
                    if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                    {
                        cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 0);
                        TH1F* validationHistogram = fDetectorPixelValidationHistograms.getObject(cBoard->getId())
                                                        ->getObject(cOpticalGroup->getId())
                                                        ->getObject(cHybrid->getId())
                                                        ->getObject(cChip->getId())
                                                        ->getSummary<HistContainer<TH1F>>()
                                                        .fTheHistogram;
                        validationHistogram->SetStats(false);
                        validationHistogram->DrawCopy();
                        gPad->SetLogy();
                    }

                }
            }
        }
    }
}

//========================================================================================================================
void DQMHistogramPixelAlive::reset(void) {}

//========================================================================================================================
void DQMHistogramPixelAlive::fillValidationPlots(DetectorDataContainer& theOccupancy)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    TH1F* cChipValidationHistogram = nullptr;
                    auto  cType                    = cChip->getFrontEndType();
                    if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                    {
                        cChipValidationHistogram = fDetectorPixelValidationHistograms.getObject(cBoard->getId())
                                                       ->getObject(cOpticalGroup->getId())
                                                       ->getObject(cHybrid->getId())
                                                       ->getObject(cChip->getId())
                                                       ->getSummary<HistContainer<TH1F>>()
                                                       .fTheHistogram;
                    }

                    uint cChannelBin = 1;
//		    uint32_t NCH = NCHANNELS;
//                    if(cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
//                        NCH = NSSACHANNELS;
//                    else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
//                        NCH = NMPACHANNELS;

                    auto cChannelContainer =
                        theOccupancy.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getChannelContainer<Occupancy>();
                    if(cChannelContainer == nullptr) continue;
                    for(auto cChannel: *cChannelContainer)
		    {
//                    for(uint32_t iChan = 0; iChan < NCH; iChan++)
//                    {
//                        float occupancy = theOccupancy.at(cBoard->getIndex())->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<Occupancy>(iChan).fOccupancy;
                        cChipValidationHistogram->SetBinContent(cChannelBin, cChannelBin, cChannel.fOccupancy);
                        cChipValidationHistogram->SetBinError(cChannelBin++, cChannel.fOccupancyError);
                    }
                }
            }
        }
    }
}


/*!
        \file                DQMHistogramPixelAlive.h
        \brief               base class to create and fill monitoring histograms
        \author              Adam Kobert
        \version             1.0
        \date                4/10/23
        Support :            mail to : 
*/

#ifndef __DQMHISTOGRAMPIXELALIVE_H__
#define __DQMHISTOGRAMPIXELALIVE_H__
#include "DQMUtils/DQMHistogramBase.h"
#include "Utils/Container.h"
#include "Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramPixelAlive
 * \brief Class for PixelAlive monitoring histograms
 */
class DQMHistogramPixelAlive : public DQMHistogramBase
{
  public:
    /*!
     * constructor
     */
    DQMHistogramPixelAlive();

    /*!
     * destructor
     */
    ~DQMHistogramPixelAlive();

    /*!
     * Book histograms
     */
    void book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_Parser::SettingsMap& pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill(std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process() override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    // virtual void summarizeHistos();

    /*!
     * \brief Fill validation histograms
     * \param theOccupancy : DataContainer for the occupancy
     */
    void fillValidationPlots(DetectorDataContainer& theOccupancy);



  private:
    DetectorContainer*    fDetectorContainer;
    uint32_t              fNPixelChannels = 0, fNStripChannels = 0;
    DetectorDataContainer fThresholdAndNoiseContainer;


    DetectorDataContainer fDetectorStripValidationHistograms;
    DetectorDataContainer fDetectorPixelValidationHistograms;

    DetectorDataContainer fDetectorData;

    bool fWithCBC = false;
    bool fWithSSA = false;
    bool fWithMPA = false;

};
#endif

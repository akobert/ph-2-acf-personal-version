#include "Tool.h"
#include <numeric>

#include "HWDescription/Chip.h"
#include "Utils/ChannelGroupHandler.h"
#include "Utils/Container.h"
#include "Utils/ContainerFactory.h"
#include "Utils/ContainerSerialization.h"
#include "Utils/Utilities.h"

#include "Utils/DataContainer.h"
#include "Utils/EmptyContainer.h"
#include "Utils/Occupancy.h"
#include <future>

#include "Utils/ConfigureInfo.h"
#include "Utils/MPAChannelGroupHandler.h"
#include "Utils/SSAChannelGroupHandler.h"
#include "Utils/StartInfo.h"

#ifdef __USE_ROOT__
#include "DQMUtils/DQMMetadataIT.h"
#include "DQMUtils/DQMMetadataOT.h"
#endif

using namespace Ph2_System;
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;

std::atomic<bool> Tool::fKeepRunning(false);

Tool::Tool()
    : SystemController()
    ,
#ifdef __USE_ROOT__
    fCanvasMap()
    , fChipHistMap()
    , fHybridHistMap()
    , fSummaryTree(nullptr)
    ,
#endif
    fType()
    , fTestGroupChannelMap()
    , fDirectoryName("")
    ,
#ifdef __USE_ROOT__
    fResultFile(nullptr)
    ,
#endif
    fSkipMaskedChannels(false)
    , fAllChan(false)
    , fMaskChannelsFromOtherGroups(false)
    , fTestPulse(false)
    , fDoBoardBroadcast(false)
    , fDoHybridBroadcast(false)
{
#ifdef __HTTP__
    fHttpServer = nullptr;
#endif
}

#ifdef __USE_ROOT__
#ifdef __HTTP__
Tool::Tool(THttpServer* pHttpServer)
    : SystemController()
    , fCanvasMap()
    , fChipHistMap()
    , fHybridHistMap()
    , fDQMMetadata(nullptr)
    , fType()
    , fTestGroupChannelMap()
    , fDirectoryName("")
    , fResultFile(nullptr)
    , fHttpServer(pHttpServer)
    , fRunNumber(0)
    , fSkipMaskedChannels(false)
    , fAllChan(false)
    , fMaskChannelsFromOtherGroups(false)
    , fTestPulse(false)
    , fDoBoardBroadcast(false)
    , fDoHybridBroadcast(false)
{
}
#endif
#endif

Tool::Tool(const Tool& pTool) { this->Inherit(&pTool); }

Tool::~Tool() {}

// void Tool::privateRunning(std::promise<int>&& thePromise)
// {
//     try
//     {
//         Running();
//         Tool::InformImDone();
//         thePromise.set_value(0);
//     }
//     catch(...)
//     {
//         Tool::InformImDone();
//         thePromise.set_value(99);
//         thePromise.set_exception(std::current_exception());
//     }
// }

bool Tool::GetRunningStatus()
{
    std::future_status runningStatus = fRunningFuture.wait_for(std::chrono::milliseconds(500u));
    if(runningStatus == std::future_status::ready || runningStatus == std::future_status::deferred)
    {
        try
        {
            if(fRunningFuture.valid()) fRunningFuture.get();
        }
        catch(const std::future_error& e)
        {
            LOG(INFO) << "Ignoring future exception, future already retrieved";
        }
        catch(const std::exception& e)
        {
            throw std::runtime_error(e.what());
        }
        return true;
    }
    else
        return false;
}

void Tool::waitForRunToBeCompleted()
{
    try
    {
        if(fRunningFuture.valid()) fRunningFuture.wait();
    }
    catch(const std::future_error& e)
    {
        LOG(INFO) << "Ignoring future exception, future already retrieved";
    }
    // std::unique_lock<std::recursive_mutex> theGuard(theMtx);
    // wakeUp.wait(theGuard, [this]() { return doExit; });
}

void Tool::Configure(const ConfigureInfo& theConfigureInfo)
{
    SystemController::Configure(theConfigureInfo);
    ConfigureCalibration();
}

void Tool::Start(const StartInfo& theStartInfo)
{
    if(fDirectoryName == "")
    {
        std::string resultDirectory = getResultDirectoryName(theStartInfo);
        CreateResultDirectory(resultDirectory, false, false);
    }
    initMetadataAndFillInitialConditions();

    // doExit       = false;
    Tool::fKeepRunning = true;
    fRunNumber         = theStartInfo.getRunNumber();
    fRunningFuture     = std::async(std::launch::async, &Tool::Running, this);
    // std::promise<int> thePromise;
    // fRunningFuture = thePromise.get_future();
    // fRunningThread = std::thread(&Tool::privateRunning, this, std::move(thePromise));
}

// void Tool::InformImDone()
// {
//     std::unique_lock<std::recursive_mutex> theGuard(theMtx);
//     doExit = true;
//     theGuard.unlock();
//     wakeUp.notify_one();
// }

void Tool::initMetadataAndFillInitialConditions()
{
    fillNameContainerWithChipIDs();

    std::string theUsername;
    try
    {
        theUsername = std::string(std::getenv("USER"));
    }
    catch(const std::exception& e)
    {
        LOG(WARNING) << e.what();
        LOG(WARNING) << __PRETTY_FUNCTION__ << " Username not set, using dummy name";
        theUsername = "user";
    }

    DetectorDataContainer theUsernameContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theUsernameContainer);
    theUsernameContainer.getSummary<std::string>() = theUsername;

    std::string           theHostName = std::string(std::getenv("HOSTNAME"));
    DetectorDataContainer theHostNameContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theHostNameContainer);
    theHostNameContainer.getSummary<std::string>() = theHostName;

    std::string           theGitCommitHash = GIT_COMMIT_HASH;
    DetectorDataContainer theGitCommitHashContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theGitCommitHashContainer);
    theGitCommitHashContainer.getSummary<std::string>() = theGitCommitHash;

    DetectorDataContainer theCalibrationNameContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theCalibrationNameContainer);
    theCalibrationNameContainer.getSummary<std::string>() = fCalibrationName;

    DetectorDataContainer theFirmwareVersionContainer;
    ContainerFactory::copyAndInitBoard<std::string>(*fDetectorContainer, theFirmwareVersionContainer);
    for(const auto board: *fDetectorContainer) theFirmwareVersionContainer.getObject(board->getId())->getSummary<std::string>() = std::to_string(fBeBoardInterface->getBoardFirmwareVersion(board));

    DetectorDataContainer theDetectorConfigurationContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theDetectorConfigurationContainer);
    theDetectorConfigurationContainer.getSummary<std::string>() = fConfigurationFileContent;

    DetectorDataContainer theCalibrationTimestampContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theCalibrationTimestampContainer);
    std::stringstream timestampStream; // for come reason std::to_string does not work with python
    timestampStream << getTimeStamp();
    theCalibrationTimestampContainer.getSummary<std::string>() = timestampStream.str();

    DetectorDataContainer theReadoutChipConfigurationContainer;
    ContainerFactory::copyAndInitChip<std::string>(*fDetectorContainer, theReadoutChipConfigurationContainer);
    fillReadoutChipConfigurationContainer(theReadoutChipConfigurationContainer);

    DetectorDataContainer theLpGBTConfigurationContainer;
    ContainerFactory::copyAndInitOpticalGroup<std::string>(*fDetectorContainer, theLpGBTConfigurationContainer);
    fillLpGBTConfigurationContainer(theLpGBTConfigurationContainer);

    DetectorDataContainer theLpGBTFuseIdContainer;
    ContainerFactory::copyAndInitOpticalGroup<std::string>(*fDetectorContainer, theLpGBTFuseIdContainer);
    fillLpGBTFuseIdContainer(theLpGBTFuseIdContainer);
    bool isInitialValue = true;

#ifdef __USE_ROOT__
    InitResultFile("Hybrid");
    if(fBoardType == BoardType::D19C) { fDQMMetadata = new DQMMetadataOT(); }
    else if(fBoardType == BoardType::RD53)
    {
        fDQMMetadata = new DQMMetadataIT();
    }
    else
    {
        LOG(ERROR) << __PRETTY_FUNCTION__ << " [" << __LINE__ << "] Board type not defined!! Impossible to create DQM for metadata, aborting..." << std::endl;
        abort();
    }
    fDQMMetadata->book(fResultFile, *fDetectorContainer, fSettingsMap);
    if(fNameContainer != nullptr) fDQMMetadata->fillObjectNames(*fNameContainer);
    fDQMMetadata->fillUsername(theUsernameContainer);
    fDQMMetadata->fillHostName(theHostNameContainer);
    fDQMMetadata->fillGitCommitHash(theGitCommitHashContainer);
    fDQMMetadata->fillFirmwareVersion(theFirmwareVersionContainer);
    fDQMMetadata->fillCalibrationName(theCalibrationNameContainer);
    fDQMMetadata->fillDetectorConfiguration(theDetectorConfigurationContainer);
    fDQMMetadata->fillCalibrationTimestamp(theCalibrationTimestampContainer, isInitialValue);
    fDQMMetadata->fillReadoutChipConfiguration(theReadoutChipConfigurationContainer, isInitialValue);
    fDQMMetadata->fillLpGBTConfiguration(theLpGBTConfigurationContainer, isInitialValue);
    fDQMMetadata->fillLpGBTFuseId(theLpGBTFuseIdContainer);
#else
    if(fDQMStreamerEnabled)
    {
        ContainerSerialization theObjectNameSerialization("MetadataObjectNames");
        theObjectNameSerialization.streamByDetectorContainer(fDQMStreamer, *fNameContainer);

        ContainerSerialization theUsernameSerialization("MetadataUsername");
        theUsernameSerialization.streamByDetectorContainer(fDQMStreamer, theUsernameContainer);

        ContainerSerialization theHostNameSerialization("MetadataHostName");
        theHostNameSerialization.streamByDetectorContainer(fDQMStreamer, theHostNameContainer);

        ContainerSerialization theGitCommitHashSerialization("MetadataGitCommitHash");
        theGitCommitHashSerialization.streamByDetectorContainer(fDQMStreamer, theGitCommitHashContainer);

        ContainerSerialization theFirmwareVersionSerialization("MetadataFirmwareVersion");
        theFirmwareVersionSerialization.streamByDetectorContainer(fDQMStreamer, theFirmwareVersionContainer);

        ContainerSerialization theCalibrationNameSerialization("MetadataCalibrationName");
        theCalibrationNameSerialization.streamByDetectorContainer(fDQMStreamer, theCalibrationNameContainer);

        ContainerSerialization theDetectorConfigurationSerialization("MetadataDetectorConfiguration");
        theDetectorConfigurationSerialization.streamByDetectorContainer(fDQMStreamer, theDetectorConfigurationContainer);

        ContainerSerialization theCalibrationTimestampSerialization("MetadataCalibrationTimestamp");
        theCalibrationTimestampSerialization.streamByDetectorContainer(fDQMStreamer, theCalibrationTimestampContainer, isInitialValue);

        ContainerSerialization theReadoutChipConfigurationSerialization("MetadataReadoutChipConfiguration");
        theReadoutChipConfigurationSerialization.streamByChipContainer(fDQMStreamer, theReadoutChipConfigurationContainer, isInitialValue);

        ContainerSerialization theLpGBTConfigurationSerialization("MetadataLpGBTConfiguration");
        theLpGBTConfigurationSerialization.streamByOpticalGroupContainer(fDQMStreamer, theLpGBTConfigurationContainer, isInitialValue);

        ContainerSerialization theLpGBTFuseIdSerialization("MetadataLpGBTFuseId");
        theLpGBTFuseIdSerialization.streamByBoardContainer(fDQMStreamer, theLpGBTFuseIdContainer);
    }
#endif

    if(fBoardType == BoardType::D19C) { fillOTMetadataInitialConditions(); }
    else if(fBoardType == BoardType::RD53)
    {
        fillITMetadataInitialConditions();
    }
}

void Tool::fillOTMetadataInitialConditions()
{
    bool                  isInitialValue = true;
    DetectorDataContainer theCICFuseIdContainer;
    ContainerFactory::copyAndInitHybrid<std::string>(*fDetectorContainer, theCICFuseIdContainer);
    fillCICFuseIdContainer(theCICFuseIdContainer);

    DetectorDataContainer theCICConfigurationContainer;
    ContainerFactory::copyAndInitHybrid<std::string>(*fDetectorContainer, theCICConfigurationContainer);
    fillCICConfigurationContainer(theCICConfigurationContainer);

#ifdef __USE_ROOT__
    auto* theOTDQMMetadata = static_cast<DQMMetadataOT*>(fDQMMetadata);
    theOTDQMMetadata->fillCICFuseId(theCICFuseIdContainer);
    theOTDQMMetadata->fillCICConfiguration(theCICConfigurationContainer, isInitialValue);
#else
    if(fDQMStreamerEnabled)
    {
        ContainerSerialization theCICFuseIdSerialization("MetadataCICFuseId");
        theCICFuseIdSerialization.streamByBoardContainer(fDQMStreamer, theCICFuseIdContainer);

        ContainerSerialization theCICConfigurationSerialization("MetadataCICConfiguration");
        theCICConfigurationSerialization.streamByHybridContainer(fDQMStreamer, theCICConfigurationContainer, isInitialValue);
    }
#endif
}

void Tool::fillITMetadataInitialConditions() {}

void Tool::fillMetadataFinalConditions()
{
    bool                  isInitialValue = false;
    DetectorDataContainer theReadoutChipConfigurationContainer;
    ContainerFactory::copyAndInitChip<std::string>(*fDetectorContainer, theReadoutChipConfigurationContainer);
    fillReadoutChipConfigurationContainer(theReadoutChipConfigurationContainer);

    DetectorDataContainer theLpGBTConfigurationContainer;
    ContainerFactory::copyAndInitOpticalGroup<std::string>(*fDetectorContainer, theLpGBTConfigurationContainer);
    fillLpGBTConfigurationContainer(theLpGBTConfigurationContainer);

    DetectorDataContainer theCalibrationTimestampContainer;
    ContainerFactory::copyAndInitDetector<std::string>(*fDetectorContainer, theCalibrationTimestampContainer);
    std::stringstream timestampStream; // for come reason std::to_string does not work with python
    timestampStream << getTimeStamp();
    theCalibrationTimestampContainer.getSummary<std::string>() = timestampStream.str();

#ifdef __USE_ROOT__
    fDQMMetadata->fillReadoutChipConfiguration(theReadoutChipConfigurationContainer, isInitialValue);
    fDQMMetadata->fillLpGBTConfiguration(theLpGBTConfigurationContainer, isInitialValue);
    fDQMMetadata->fillCalibrationTimestamp(theCalibrationTimestampContainer, isInitialValue);
#else
    if(fDQMStreamerEnabled)
    {
        ContainerSerialization theReadoutChipConfigurationSerialization("MetadataReadoutChipConfiguration");
        theReadoutChipConfigurationSerialization.streamByChipContainer(fDQMStreamer, theReadoutChipConfigurationContainer, isInitialValue);

        ContainerSerialization theLpGBTConfigurationSerialization("MetadataLpGBTConfiguration");
        theLpGBTConfigurationSerialization.streamByOpticalGroupContainer(fDQMStreamer, theLpGBTConfigurationContainer, isInitialValue);

        ContainerSerialization theCalibrationTimestampSerialization("MetadataCalibrationTimestamp");
        theCalibrationTimestampSerialization.streamByDetectorContainer(fDQMStreamer, theCalibrationTimestampContainer, isInitialValue);
    }
#endif

    if(fBoardType == BoardType::D19C) { fillOTMetadataFinalConditions(); }
    else if(fBoardType == BoardType::RD53)
    {
        fillITMetadataFinalConditions();
    }
}

void Tool::fillITMetadataFinalConditions() {}

void Tool::fillOTMetadataFinalConditions()
{
    bool isInitialValue = false;

    DetectorDataContainer theCICConfigurationContainer;
    ContainerFactory::copyAndInitHybrid<std::string>(*fDetectorContainer, theCICConfigurationContainer);
    fillCICConfigurationContainer(theCICConfigurationContainer);

#ifdef __USE_ROOT__
    auto* theOTDQMMetadata = static_cast<DQMMetadataOT*>(fDQMMetadata);
    theOTDQMMetadata->fillCICConfiguration(theCICConfigurationContainer, isInitialValue);
#else
    if(fDQMStreamerEnabled)
    {
        std::cout << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << std::endl;

        ContainerSerialization theCICConfigurationSerialization("MetadataCICConfiguration");
        theCICConfigurationSerialization.streamByHybridContainer(fDQMStreamer, theCICConfigurationContainer, isInitialValue);
    }
#endif
}

void Tool::fillNameContainerWithChipIDs()
{
    if(fNameContainer == nullptr) return;
    for(auto cBoard: *fDetectorContainer)
    {
        fNameContainer->getObject(cBoard->getId())->getSummary<std::string>() = cBoard->getConnectionUri();
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    uint32_t chipFuseId = fReadoutChipInterface->ReadChipFuseID(cChip);
                    fNameContainer->getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getSummary<std::string>() =
                        std::to_string(chipFuseId);
                }
            }
        }
    }
}

void Tool::fillReadoutChipConfigurationContainer(DetectorDataContainer& theReadoutChipConfigurationContainer)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    theReadoutChipConfigurationContainer.getObject(cBoard->getId())
                        ->getObject(cOpticalGroup->getId())
                        ->getObject(cHybrid->getId())
                        ->getObject(cChip->getId())
                        ->getSummary<std::string, EmptyContainer>() = cChip->getRegMapStream().str();
                }
            }
        }
    }
}

void Tool::fillCICFuseIdContainer(DetectorDataContainer& theCICFuseIdContainer)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                uint32_t chipFuseId = fCicInterface->ReadChipFuseID(static_cast<OuterTrackerHybrid*>(cHybrid)->fCic);
                theCICFuseIdContainer.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::string>() = std::to_string(chipFuseId);
            }
        }
    }
}

void Tool::fillCICConfigurationContainer(DetectorDataContainer& theCICConfigurationContainer)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                theCICConfigurationContainer.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getSummary<std::string>() =
                    static_cast<OuterTrackerHybrid*>(cHybrid)->fCic->getRegMapStream().str();
            }
        }
    }
}

void Tool::fillLpGBTConfigurationContainer(DetectorDataContainer& theLpGBTConfigurationContainer)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            auto theLpGBT = cOpticalGroup->flpGBT;
            if(theLpGBT == nullptr) continue;
            theLpGBTConfigurationContainer.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getSummary<std::string, EmptyContainer>() = theLpGBT->getRegMapStream().str();
        }
    }
}

void Tool::fillLpGBTFuseIdContainer(DetectorDataContainer& theLpGBTFuseIdContainer)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            auto theLpGBT = cOpticalGroup->flpGBT;
            if(theLpGBT == nullptr) continue;
            uint32_t chipFuseId                                                                                                              = flpGBTInterface->ReadChipFuseID(theLpGBT);
            theLpGBTFuseIdContainer.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getSummary<std::string, EmptyContainer>() = std::to_string(chipFuseId);
        }
    }
}

void Tool::Stop()
{
    if(Tool::fKeepRunning == true)
    {
        Tool::fKeepRunning = false;
        Tool::waitForRunToBeCompleted();
        // if(fRunningThread.joinable() == true) fRunningThread.join();
        try
        {
            if(fRunningFuture.valid()) fRunningFuture.get();
        }
        catch(const std::future_error& e)
        {
            LOG(INFO) << "Ignoring future exception, future already retrieved";
        }
        catch(const std::exception& e)
        {
            throw std::runtime_error(e.what());
        }
        SystemController::Stop();

        fillMetadataFinalConditions();
        if(fDQMStreamerEnabled)
        {
            std::string  doneWithRunMessage = END_OF_TRANSMISSION_MESSAGE;
            PacketHeader thePacketHeader;
            thePacketHeader.addPacketHeader(doneWithRunMessage);
            fDQMStreamer->broadcast(doneWithRunMessage);
        }
        Tool::dumpConfigFiles();
        Tool::SaveResults();
        Tool::WriteRootFile();
        Tool::CloseResultFile();
    }
}

void Tool::Inherit(const Tool* pTool)
{
    SystemController::Inherit(pTool);

#ifdef __USE_ROOT__
    fResultFile = pTool->fResultFile;
#endif
    fType          = pTool->fType;
    fDirectoryName = pTool->fDirectoryName;
#ifdef __USE_ROOT__
    fSummaryTree          = pTool->fSummaryTree;
    fCanvasMap            = pTool->fCanvasMap;
    fChipHistMap          = pTool->fChipHistMap;
    fHybridHistMap        = pTool->fHybridHistMap;
    fBeBoardHistMap       = pTool->fBeBoardHistMap;
    fSummaryTreeParameter = pTool->fSummaryTreeParameter;
    fSummaryTreeValue     = pTool->fSummaryTreeValue;
    fDQMMetadata          = pTool->fDQMMetadata;
#endif
    fTestGroupChannelMap = pTool->fTestGroupChannelMap;
    fRunNumber           = pTool->fRunNumber;
    // fRunningFuture               = pTool->fRunningFuture;
    fSkipMaskedChannels          = pTool->fSkipMaskedChannels;
    fAllChan                     = pTool->fAllChan;
    fMaskForTestGroupChannelMap  = pTool->fMaskForTestGroupChannelMap;
    fMaskChannelsFromOtherGroups = pTool->fMaskChannelsFromOtherGroups;
    fTestPulse                   = pTool->fTestPulse;
    fDoBoardBroadcast            = pTool->fDoBoardBroadcast;
    fDoHybridBroadcast           = pTool->fDoHybridBroadcast;
    fDirectoryName               = pTool->fDirectoryName;
    fResultFileName              = pTool->fResultFileName;
    fUseReadNEvents              = pTool->fUseReadNEvents;
    fNReadbackEvents             = pTool->fNReadbackEvents;
    fNormalize                   = pTool->fNormalize;

#ifdef __HTTP__
    fHttpServer = pTool->fHttpServer;
#endif
}

void Tool::Inherit(const SystemController* pSystemController) { SystemController::Inherit(pSystemController); }

void Tool::resetPointers() {}

void Tool::Destroy()
{
    LOG(INFO) << BOLDRED << "Destroying memory objects" << RESET;
#ifdef __HTTP__
    LOG(INFO) << BOLDRED << "Destroying HttpServer" << RESET;
    if(fHttpServer)
    {
        delete fHttpServer;
        fHttpServer = nullptr;
    }
    LOG(INFO) << BOLDRED << "HttpServer Destroyed" << RESET;
#endif

    SoftDestroy();
    LOG(INFO) << BOLDRED << "Memory objects destroyed" << RESET;
    SystemController::Destroy();
}

void Tool::SoftDestroy()
{
#ifdef __USE_ROOT__
    if(fResultFile != nullptr)
    {
        if(fResultFile->IsOpen()) fResultFile->Close();

        if(fResultFile)
        {
            delete fResultFile;
            fResultFile = nullptr;
        }
    }

    for(auto canvas: fCanvasMap)
    {
        delete canvas.second;
        canvas.second = nullptr;
    }
    fCanvasMap.clear();
    for(auto chip: fChipHistMap)
    {
        for(auto hist: chip.second)
        {
            delete hist.second;
            hist.second = nullptr;
        }
    }
    fChipHistMap.clear();
    for(auto chip: fHybridHistMap)
    {
        for(auto hist: chip.second)
        {
            delete hist.second;
            hist.second = nullptr;
        }
    }
    fHybridHistMap.clear();
    for(auto chip: fBeBoardHistMap)
    {
        for(auto hist: chip.second)
        {
            delete hist.second;
            hist.second = nullptr;
        }
    }
    fBeBoardHistMap.clear();

    delete fDQMMetadata;
    fDQMMetadata = nullptr;

#endif
    fTestGroupChannelMap.clear();
}

#ifdef __USE_ROOT__
std::string Tool::fSummaryTreeParameter = "";
double      Tool::fSummaryTreeValue     = 0.0;

/*!
 * \brief Initialize a 'summary' TTree in the ROOT File, with branches 'parameter'(string) and 'value'(double)
 */
void Tool::bookSummaryTree()
{
    fResultFile->cd();
    fSummaryTreeParameter = "";
    fSummaryTreeValue     = 0;
    fSummaryTree          = new TTree("summaryTree", "Most relevant results");
    fSummaryTree->Branch("Parameter", &fSummaryTreeParameter);
    fSummaryTree->Branch("Value", &fSummaryTreeValue);
}

/*!
 * \brief Insert data into the summary tree
 * \param cParameter : Name of the measurement to be stored
 * \param cValue: Value of the measurement to be stored
 */
void Tool::fillSummaryTree(std::string cParameter, Double_t cValue) // MINE
{
    // TString currentDirectory = getDirectoryName();
    // const char* currentDirectory = gDirectory->GetPath();
    fResultFile->cd();
    fSummaryTreeParameter.clear();
    TString cParameter_TString(cParameter);
    fSummaryTreeParameter = cParameter_TString;
    fSummaryTreeValue     = cValue;
    if(fSummaryTree) fSummaryTree->Fill();
    // fResultFile->cd(currentDirectory);
}

Double_t Tool::getSummaryParameter(std::string cParameter)
{
    for(int i = 0; i < fSummaryTree->GetEntries(); i++)
    {
        fSummaryTree->GetEntry(i);
        if(fSummaryTreeParameter == cParameter) return fSummaryTreeValue;
    }
    return -1.0;
}

void Tool::bookHistogram(ChipContainer* pChip, std::string pName, TObject* pObject)
{
    TH1* tmpHistogramPointer = dynamic_cast<TH1*>(pObject);
    if(tmpHistogramPointer != nullptr) tmpHistogramPointer->SetDirectory(0);

    // Find or create map<string,TOBject> for specific CBC
    auto cChipHistMap = fChipHistMap.find(pChip);

    if(cChipHistMap == std::end(fChipHistMap))
    {
        // Fabio: CBC specific -> to be moved out from Tool
        LOG(INFO) << "Histo Map for CBC " << int(pChip->getId()) << " (FE " << int(static_cast<ReadoutChip*>(pChip)->getHybridId()) << ") does not exist - creating ";
        std::map<std::string, TObject*> cTempChipMap;

        fChipHistMap[pChip] = cTempChipMap;
        cChipHistMap        = fChipHistMap.find(pChip);
    }

    // Find histogram with given name: if it exists, delete the object, if not create
    auto cHisto = cChipHistMap->second.find(pName);

    if(cHisto != std::end(cChipHistMap->second)) cChipHistMap->second.erase(cHisto);

    cChipHistMap->second[pName] = pObject;

#ifdef __HTTP__
    if(fHttpServer) fHttpServer->Register("/Histograms", pObject);
#endif
}

void Tool::bookHistogram(HybridContainer* pHybrid, std::string pName, TObject* pObject)
{
    TH1* tmpHistogramPointer = dynamic_cast<TH1*>(pObject);
    if(tmpHistogramPointer != nullptr) tmpHistogramPointer->SetDirectory(0);

    // Find or create map<string,TOBject> for specific CBC
    auto cHybridHistMap = fHybridHistMap.find(pHybrid);

    if(cHybridHistMap == std::end(fHybridHistMap))
    {
        LOG(INFO) << "Histo Map for Hybrid " << int(pHybrid->getId()) << " does not exist - creating ";
        std::map<std::string, TObject*> cTempHybridMap;

        fHybridHistMap[pHybrid] = cTempHybridMap;
        cHybridHistMap          = fHybridHistMap.find(pHybrid);
    }

    // Find histogram with given name: if it exists, delete the object, if not create
    auto cHisto = cHybridHistMap->second.find(pName);

    if(cHisto != std::end(cHybridHistMap->second)) cHybridHistMap->second.erase(cHisto);

    cHybridHistMap->second[pName] = pObject;
#ifdef __HTTP__
    if(fHttpServer) fHttpServer->Register("/Histograms", pObject);
#endif
}

void Tool::bookHistogram(BoardContainer* pBeBoard, std::string pName, TObject* pObject)
{
    TH1* tmpHistogramPointer = dynamic_cast<TH1*>(pObject);
    if(tmpHistogramPointer != nullptr) tmpHistogramPointer->SetDirectory(0);

    // Find or create map<string,TOBject> for specific CBC
    auto cBeBoardHistMap = fBeBoardHistMap.find(pBeBoard);

    if(cBeBoardHistMap == std::end(fBeBoardHistMap))
    {
        LOG(INFO) << "Histo Map for Hybrid " << int(pBeBoard->getId()) << " does not exist - creating ";
        std::map<std::string, TObject*> cTempHybridMap;

        fBeBoardHistMap[pBeBoard] = cTempHybridMap;
        cBeBoardHistMap           = fBeBoardHistMap.find(pBeBoard);
    }

    // Find histogram with given name: if it exists, delete the object, if not create
    auto cHisto = cBeBoardHistMap->second.find(pName);

    if(cHisto != std::end(cBeBoardHistMap->second)) cBeBoardHistMap->second.erase(cHisto);

    cBeBoardHistMap->second[pName] = pObject;
#ifdef __HTTP__
    if(fHttpServer) fHttpServer->Register("/Histograms", pObject);
#endif
}

TObject* Tool::getHist(ChipContainer* pChip, std::string pName)
{
    auto cChipHistMap = fChipHistMap.find(pChip);

    if(cChipHistMap == std::end(fChipHistMap))
    {
        // Fabio: CBC specific -> to be moved out from Tool
        LOG(ERROR) << RED << "Error: could not find the Histograms for CBC " << int(pChip->getId()) << " (FE " << int(static_cast<ReadoutChip*>(pChip)->getHybridId()) << ")" << RESET;
        return nullptr;
    }
    else
    {
        auto cHisto = cChipHistMap->second.find(pName);

        if(cHisto == std::end(cChipHistMap->second))
        {
            LOG(ERROR) << RED << "Error: could not find the Histogram with the name " << pName << RESET;
            return nullptr;
        }
        else
            return cHisto->second;
    }
}

TObject* Tool::getHist(HybridContainer* pHybrid, std::string pName)
{
    auto cHybridHistMap = fHybridHistMap.find(pHybrid);

    if(cHybridHistMap == std::end(fHybridHistMap))
    {
        LOG(ERROR) << RED << "Error: could not find the Histograms for Hybrid " << int(pHybrid->getId()) << RESET;
        return nullptr;
    }
    else
    {
        auto cHisto = cHybridHistMap->second.find(pName);

        if(cHisto == std::end(cHybridHistMap->second))
        {
            LOG(ERROR) << RED << "Error: could not find the Histogram with the name " << pName << RESET;
            return nullptr;
        }
        else
            return cHisto->second;
    }
}

TObject* Tool::getHist(BoardContainer* pBeBoard, std::string pName)
{
    auto cBeBoardHistMap = fBeBoardHistMap.find(pBeBoard);

    if(cBeBoardHistMap == std::end(fBeBoardHistMap))
    {
        LOG(ERROR) << RED << "Error: could not find the Histograms for Hybrid " << int(pBeBoard->getId()) << RESET;
        return nullptr;
    }
    else
    {
        auto cHisto = cBeBoardHistMap->second.find(pName);

        if(cHisto == std::end(cBeBoardHistMap->second))
        {
            LOG(ERROR) << RED << "Error: could not find the Histogram with the name " << pName << RESET;
            return nullptr;
        }
        else
            return cHisto->second;
    }
}
#endif

void Tool::WriteRootFile()
{
#ifdef __USE_ROOT__
    if((fResultFile != nullptr) && (fResultFile->IsOpen() == true)) fResultFile->Write();
#endif
}

void Tool::SaveResults()
{
#ifdef __USE_ROOT__
    for(const auto& cBeBoard: fBeBoardHistMap)
    {
        fResultFile->cd();

        for(const auto& cHist: cBeBoard.second) cHist.second->Write(cHist.second->GetName(), TObject::kOverwrite);

        fResultFile->cd();
    }

    // Now per FE
    for(const auto& cHybrid: fHybridHistMap)
    {
        TString  cDirName = Form("FE%d", cHybrid.first->getId());
        TObject* cObj     = gROOT->FindObject(cDirName);

        if(!cObj) fResultFile->mkdir(cDirName);

        fResultFile->cd(cDirName);

        for(const auto& cHist: cHybrid.second) cHist.second->Write(cHist.second->GetName(), TObject::kOverwrite);

        fResultFile->cd();
    }

    for(const auto& cChip: fChipHistMap)
    {
        std::string cDescr = "";
        auto        cType  = static_cast<ReadoutChip*>(cChip.first)->getFrontEndType();
        if(cType == FrontEndType::CBC3) cDescr = "CBC";
        if(cType == FrontEndType::SSA) cDescr = "SSA";
        if(cType == FrontEndType::SSA2) cDescr = "SSA2";
        if(cType == FrontEndType::MPA) cDescr = "MPA";
        if(cType == FrontEndType::MPA2) cDescr = "MPA2";

        // Fabio: CBC specific -> to be moved out from Tool
        TString  cDirName = Form("Hybrid%d%s%d", static_cast<ReadoutChip*>(cChip.first)->getHybridId(), cDescr.c_str(), cChip.first->getId());
        TObject* cObj     = gROOT->FindObject(cDirName);

        if(!cObj) fResultFile->mkdir(cDirName);

        fResultFile->cd(cDirName);

        for(const auto& cHist: cChip.second) cHist.second->Write(cHist.second->GetName(), TObject::kOverwrite);

        fResultFile->cd();
    }

    // Save Canvasses too
    for(const auto& cCanvas: fCanvasMap)
    {
        cCanvas.second->Write(cCanvas.second->GetName(), TObject::kOverwrite);
        std::string cPdfName = fDirectoryName + "/" + cCanvas.second->GetName() + ".pdf";
        cCanvas.second->SaveAs(cPdfName.c_str());
    }
    // Save summary TTree
    // fResultFile->cd();
    if((fResultFile != nullptr) && (fResultFile->IsOpen() == true)) fResultFile->cd();
    if(fSummaryTree != nullptr) fSummaryTree->Write(); // Seems to be needed with ROOT6, seems to break with ROOT5...
#endif
}

void Tool::CreateResultDirectory(const std::string& pDirname, bool pMode, bool pDate)
{
    std::string nDirname;

    if(std::getenv("GIPHT_RESULT_FOLDER"))
    {
        LOG(INFO) << "OT Module GUI (GIPHT) result directory environmental variable set: " << std::getenv("GIPHT_RESULT_FOLDER");
        nDirname = std::getenv("GIPHT_RESULT_FOLDER");
    }
    else
    {
        nDirname = pDirname;
    }
    if(pDate) nDirname += currentDateTime();

    std::string cCommand = "mkdir -p " + nDirname;

    try
    {
        system(cCommand.c_str());
    }
    catch(std::exception& e)
    {
        LOG(ERROR) << BOLDRED << "Exceptin when trying to create Result Directory: " << e.what() << RESET;
    }

    fDirectoryName = nDirname;
}

/*!
 * \brief Initialize the result Root file
 * \param pFilename : Root filename
 */
#ifdef __USE_ROOT__
void Tool::InitResultFile(const std::string& pFilename)
{
    if(fResultFile != nullptr) return;
    if(!fDirectoryName.empty())
    {
        std::string cFilename = fDirectoryName + "/" + pFilename + ".root";

        try
        {
            fResultFile     = TFile::Open(cFilename.c_str(), "RECREATE");
            fResultFileName = cFilename;
            // AddMetadata();
        }
        catch(std::exception& e)
        {
            LOG(ERROR) << "Exceptin when trying to create Result File: " << e.what();
        }
    }
    else
        LOG(INFO) << RED << "ERROR: " << RESET << "No result directory initialized - not saving results!";
}
#endif

void Tool::CloseResultFile()
{
#ifdef __USE_ROOT__
    if(fResultFile != nullptr)
    {
        LOG(INFO) << GREEN << "Closing result file: " << BOLDYELLOW << fResultFileName << RESET;
        fResultFile->Close();
        delete fResultFile;
        fResultFile = nullptr;
    }
#endif
}

#ifdef __USE_ROOT__
void Tool::AddMetadata()
{
    fResultFile->cd();
    TTree* t = new TTree();
    t->SetName("metadata");

    // save username
    std::string user;
    try
    {
        user = std::string(std::getenv("USER"));
    }
    catch(const std::exception& e)
    {
        LOG(WARNING) << e.what();
        LOG(WARNING) << __PRETTY_FUNCTION__ << " Username not set, using dummy name";
        user = "user";
    }
    t->Branch("username", &user);

    // save chip IDs
    std::vector<uint32_t> chipIds;

    uint16_t chipVectorSize = 0;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup) { chipVectorSize += cHybrid->size(); }
        }
    }
    chipIds.reserve(chipVectorSize); // Need to be done to avoid jumps of the vector memory that messes with the pointer associated to the branch

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    std::string label = getReadoutChipString(cBoard->getId(), cOpticalGroup->getId(), cHybrid->getId(), cChip->getId());

                    uint32_t value = fReadoutChipInterface->ReadChipFuseID(cChip);

                    chipIds.push_back(value);
                    // this is ok because we will only set one value per branch
                    t->Branch(label.c_str(), &chipIds.back());
                }
            }
        }
    }

    t->Fill();

    t->Write();
    fResultFile->Write();
}

void Tool::StartHttpServer(const int pPort, bool pReadonly)
{
#ifdef __HTTP__

    if(fHttpServer)
    {
        delete fHttpServer;
        fHttpServer = nullptr;
    }

    char hostname[HOST_NAME_MAX];

    try
    {
        fHttpServer = new THttpServer(Form("http:%d", pPort));
        fHttpServer->SetReadOnly(pReadonly);
        fHttpServer->SetTimer(0, kTRUE);
        fHttpServer->SetJSROOT("https://root.cern.ch/js/latest/");

        // Configure the server
        // see: https://root.cern.ch/gitweb/?p=root.git;a=blob_plain;f=tutorials/http/httpcontrol.C;hb=HEAD
        fHttpServer->SetItemField("/", "_monitoring", "5000");
        fHttpServer->SetItemField("/", "_layout", "grid2x2");

        gethostname(hostname, HOST_NAME_MAX);
    }
    catch(std::exception& e)
    {
        LOG(ERROR) << "Exception when trying to start THttpServer: " << e.what();
    }

    LOG(INFO) << "Opening THttpServer on port " << pPort << ". Point your browser to: " << GREEN << hostname << ":" << pPort << RESET;
#else
    LOG(INFO) << "Error, ROOT version < 5.34 detected or not compiled with Http Server support!"
              << " No THttpServer available! - The webgui will fail to show plots!";
    LOG(INFO) << "ROOT must be built with '--enable-http' flag to use this feature.";
#endif
}

void Tool::HttpServerProcess()
{
#ifdef __HTTP__

    if(fHttpServer)
    {
        gSystem->ProcessEvents();
        fHttpServer->ProcessRequests();
    }

#endif
}
#endif

void Tool::dumpConfigFiles()
{
    if(!fDirectoryName.empty())
    {
        // Fabio: CBC specific -> to be moved out from Tool
        for(auto board: *fDetectorContainer)
        {
            if(board->getBoardType() == BoardType::RD53) break;

            for(auto opticalGroup: *board)
            {
                auto& clpGBT = opticalGroup->flpGBT;
                if(clpGBT != nullptr)
                {
                    auto cRegMap = clpGBT->getRegMap();
                    for(auto cItemInMap: cRegMap)
                    {
                        auto cReg = flpGBTInterface->ReadChipReg(clpGBT, cItemInMap.first);
                        clpGBT->setReg(cItemInMap.first, cReg);
                    }
                    std::string cFilename = fDirectoryName + "/BE" + std::to_string(board->getId()) + "_OG" + std::to_string(opticalGroup->getId()) + "_lpGBT" + std::to_string(clpGBT->getId());
                    cFilename += ".txt";
                    clpGBT->saveRegMap(cFilename.data());
                }

                for(auto hybrid: *opticalGroup)
                {
                    for(auto chip: *hybrid)
                    {
                        std::string cFilename = fDirectoryName + "/BE" + std::to_string(board->getId()) + "_OG" + std::to_string(opticalGroup->getId()) + "_FE" + std::to_string(hybrid->getId()) +
                                                "_Chip" + std::to_string(chip->getId());
                        LOG(DEBUG) << BOLDBLUE << "Dumping readout chip configuration to " << cFilename << RESET;
                        if(chip->getFrontEndType() == FrontEndType::SSA || chip->getFrontEndType() == FrontEndType::SSA2) cFilename += "SSA";
                        cFilename += ".txt";
                        chip->saveRegMap(cFilename.data());
                    }
                    auto& cCic = static_cast<OuterTrackerHybrid*>(hybrid)->fCic;
                    if(cCic != NULL)
                    {
                        std::string cFilename =
                            fDirectoryName + "/BE" + std::to_string(board->getId()) + "_OG" + std::to_string(opticalGroup->getId()) + "_FE" + std::to_string(hybrid->getId()) + ".txt";
                        LOG(INFO) << BOLDBLUE << "Dumping CIC configuration to " << cFilename << RESET;
                        cCic->saveRegMap(cFilename.data());
                    }
                }
            }
        }
        LOG(INFO) << BOLDBLUE << "Configfiles for all Chips written to " << fDirectoryName << RESET;
    }
    else
        LOG(ERROR) << "Error: no results Directory initialized" << RESET;
}

void Tool::setSystemTestPulse(uint8_t pTPAmplitude, uint8_t pTestGroup, bool pTPState, bool pHoleMode)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    // Fabio: CBC specific but not used by common scans - BEGIN
                    // first, get the Amux Value
                    uint8_t cOriginalAmuxValue;
                    cOriginalAmuxValue = cChip->getReg("MiscTestPulseCtrl&AnalogMux");
                    // uint8_t cOriginalHitDetectSLVSValue = cChip->getReg ("HitDetectSLVS" );

                    std::vector<std::pair<std::string, uint16_t>> cRegVec;
                    uint8_t                                       cRegValue = to_reg(0, pTestGroup);

                    if(cChip->getFrontEndType() == FrontEndType::CBC3)
                    {
                        uint8_t cTPRegValue;

                        if(pTPState)
                            cTPRegValue = (cOriginalAmuxValue | 0x1 << 6);
                        else if(!pTPState)
                            cTPRegValue = (cOriginalAmuxValue & ~(0x1 << 6));

                        // uint8_t cHitDetectSLVSValue = (cOriginalHitDetectSLVSValue & ~(0x1 << 6));

                        // cRegVec.push_back ( std::make_pair ( "HitDetectSLVS", cHitDetectSLVSValue ) );
                        cRegVec.push_back(std::make_pair("MiscTestPulseCtrl&AnalogMux", cTPRegValue));
                        cRegVec.push_back(std::make_pair("TestPulseDel&ChanGroup", cRegValue));
                        cRegVec.push_back(std::make_pair("TestPulsePotNodeSel", pTPAmplitude));
                        LOG(DEBUG) << BOLDBLUE << "Read original Amux Value to be: " << std::bitset<8>(cOriginalAmuxValue) << " and changed to " << std::bitset<8>(cTPRegValue)
                                   << " - the TP is bit 6!" RESET;
                    }

                    this->fReadoutChipInterface->WriteChipMultReg(cChip, cRegVec);
                    // Fabio: CBC specific but not used by common scans - END
                }
            }
        }
    }
}

void Tool::enableTestPulse(bool enableTP)
{
    fTestPulse = enableTP;
    if(enableTP) setFWTestPulse();
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { fReadoutChipInterface->enableInjection(cChip, enableTP); }
            }
        }
    }
}

void Tool::selectGroupTestPulse(Chip* cChip, uint8_t pTestGroup)
{
    switch(cChip->getFrontEndType())
    {
    case FrontEndType::CBC3:
    {
        uint8_t cRegValue = to_reg(0, pTestGroup);
        this->fReadoutChipInterface->WriteChipReg(cChip, "TestPulseDel&ChanGroup", cRegValue);
        break;
    }

    default:
    {
        LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " FrontEnd type not recognized for Bebord " << cChip->getBeBoardId() << " Hybrid " << cChip->getHybridId() << " Chip " << +cChip->getId()
                   << ", aborting" << RESET;
        throw("[Tool::selectGroupTestPulse]\tError, FrontEnd type not found");
        break;
    }
    }
}

void Tool::setFWTestPulse()
{
    for(auto cBoard: *fDetectorContainer)
    {
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        switch(cBoard->getBoardType())
        {
        case BoardType::D19C:
        {
            EventType cEventType = cBoard->getEventType();
            bool      cAsync     = (cEventType == EventType::PSAS);
            if(!cAsync)
            {
                cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 6});
                cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
            }
            else
            {
                LOG(INFO) << BOLDBLUE << "Since I'm in ASYNC mode .. set trigger source to 12" << RESET;
                cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 12});
                cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
            }
            break;
        }

        default:
        {
            LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " BeBoard type not recognized for Bebord " << cBoard->getId() << ", aborting" << RESET;
            throw("[Tool::setFWTestPulse]\tError, BeBoard type not found");
            break;
        }
        }

        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
    }
}

void Tool::CreateReport()
{
    std::ofstream report;
    report.open(fDirectoryName + "/TestReport.txt", std::ofstream::out | std::ofstream::app);
    report.close();
}

void Tool::AmmendReport(std::string pString)
{
    std::ofstream report;
    report.open(fDirectoryName + "/TestReport.txt", std::ofstream::out | std::ofstream::app);
    report << pString << std::endl;
    report.close();
}

std::pair<float, float> Tool::getStats(std::vector<float> pData)
{
    float              cMean = std::accumulate(pData.begin(), pData.end(), 0.) / pData.size();
    std::vector<float> cTmp(pData.size(), 0);
    std::transform(pData.begin(), pData.end(), cTmp.begin(), [&](float el) { return (el - cMean) * (el - cMean); });
    float cStandardDeviation = std::sqrt(std::accumulate(cTmp.begin(), cTmp.end(), 0.) / (cTmp.size() - 1.));
    return std::make_pair(cMean, cStandardDeviation);
}

std::pair<std::vector<float>, std::vector<float>> Tool::getDerivative(std::vector<float> pData, std::vector<float> pValues, bool pIgnoreNegative)
{
    std::vector<float> cWeights(pData.size());
    std::adjacent_difference(pData.begin(), pData.end(), cWeights.begin());
    // replace negative entries with 0s
    if(pIgnoreNegative) std::replace_if(cWeights.begin(), cWeights.end(), [](float i) { return std::signbit(i); }, 0);
    cWeights.erase(cWeights.begin(), cWeights.begin() + 1);
    pValues.erase(pValues.begin(), pValues.begin() + 1);
    return std::make_pair(cWeights, pValues);
}

std::pair<float, float> Tool::evalNoise(std::vector<float> pData, std::vector<float> pValues, bool pIgnoreNegative)
{
    std::vector<float> cWeights(pData.size());
    std::adjacent_difference(pData.begin(), pData.end(), cWeights.begin());
    cWeights.erase(cWeights.begin(), cWeights.begin() + 1);
    if(pIgnoreNegative) std::replace_if(cWeights.begin(), cWeights.end(), [](float i) { return std::signbit(i); }, 0);
    float cN            = static_cast<float>(cWeights.size() - std::count(cWeights.begin(), cWeights.end(), 0.));
    float cSumOfWeights = std::accumulate(cWeights.begin(), cWeights.end(), 0.);
    // Weighted sum of scan values to get pedestal
    pData.erase(pData.begin(), pData.begin() + 1);
    std::fill(pData.begin(), pData.end(), 0.);
    std::transform(cWeights.begin(), cWeights.end(), pValues.begin(), pData.begin(), std::multiplies<float>());
    float cMean = std::accumulate(pData.begin(), pData.end(), 0.);
    cMean /= cSumOfWeights;
    // Weighted sample variance of scan values to get noise
    std::transform(pValues.begin(), pValues.end(), pData.begin(), [&](float el) { return (el - cMean) * (el - cMean); });
    std::transform(cWeights.begin(), cWeights.end(), pData.begin(), pData.begin(), std::multiplies<float>());
    float cCorrection = std::sqrt(cN / (cN - 1.));
    float cNoise      = (std::accumulate(pData.begin(), pData.end(), 0.) / (cSumOfWeights));
    cNoise            = std::sqrt(cNoise);
    LOG(DEBUG) << BOLDBLUE << "Noise is " << cNoise << " -- sum of weights  " << cSumOfWeights << " " << cN << " non zero-elements : correction of " << cCorrection << RESET;
    return std::make_pair(cMean, cNoise * cCorrection);
}

// Then a method to un-mask pairs of channels on a given CBC
void Tool::unmaskPair(Chip* cChip, std::pair<uint8_t, uint8_t> pPair)
{
    // Fabio: CBC specific but not used by common scans - BEGIN

    // Get ready to mask/un-mask channels in pairs...
    MaskedChannelsList cMaskedList;
    MaskedChannels     cMaskedChannels;
    cMaskedChannels.clear();
    cMaskedChannels.push_back(pPair.first);

    uint8_t     cRegisterIndex = pPair.first >> 3;
    std::string cMaskRegName   = fChannelMaskMapCBC3[cRegisterIndex];
    cMaskedList.insert(std::pair<std::string, MaskedChannels>(cMaskRegName.c_str(), cMaskedChannels));

    cRegisterIndex = pPair.second >> 3;
    cMaskRegName   = fChannelMaskMapCBC3[cRegisterIndex];
    auto it        = cMaskedList.find(cMaskRegName.c_str());
    if(it != cMaskedList.end()) { (it->second).push_back(pPair.second); }
    else
    {
        cMaskedChannels.clear();
        cMaskedChannels.push_back(pPair.second);
        cMaskedList.insert(std::pair<std::string, MaskedChannels>(cMaskRegName.c_str(), cMaskedChannels));
    }

    // Do the actual channel un-masking
    for(auto cMasked: cMaskedList)
    {
        uint8_t     cRegValue = 0; // cChip->getReg (cMasked.first);
        std::string cOutput   = "";
        for(auto cMaskedChannel: cMasked.second)
        {
            uint8_t cBitShift = (cMaskedChannel)&0x7;
            cRegValue |= (1 << cBitShift);
            std::string cChType = ((+cMaskedChannel & 0x1) == 0) ? "seed" : "correlation";
            std::string cOut    = "Channel " + std::to_string((int)cMaskedChannel) + " in the " + cChType.c_str() + " layer\t";
            cOutput += cOut.data();
        }
        // Channels for stub sweep : " << cOutput.c_str() << RESET ;
        fReadoutChipInterface->WriteChipReg(cChip, cMasked.first, cRegValue);
    }
    // Fabio: CBC specific but not used by common scans - END
}

uint16_t Tool::getMaxNumberOfGroups()
{
    uint16_t maxNumberOfGroups = 0;
    for(const auto board: *fDetectorContainer)
    {
        for(const auto opticalGroup: *board)
        {
            for(const auto hybrid: *opticalGroup)
            {
                for(const auto chip: *hybrid)
                {
                    uint16_t numberOfGroups = getChannelGroupHandlerContainer()
                                                  ->getObject(board->getId())
                                                  ->getObject(opticalGroup->getId())
                                                  ->getObject(hybrid->getId())
                                                  ->getObject(chip->getId())
                                                  ->getSummary<std::shared_ptr<ChannelGroupHandler>>()
                                                  ->getNumberOfGroups();
                    if(numberOfGroups > maxNumberOfGroups) maxNumberOfGroups = numberOfGroups;
                }
            }
        }
    }

    return maxNumberOfGroups;
}

// Two dimensional dac scan
void Tool::scanDacDac(const std::string&                               dac1Name,
                      const std::vector<uint16_t>&                     dac1List,
                      const std::string&                               dac2Name,
                      const std::vector<uint16_t>&                     dac2List,
                      uint32_t                                         numberOfEvents,
                      std::vector<std::vector<DetectorDataContainer*>> detectorContainerVectorOfVector,
                      int32_t                                          numberOfEventsPerBurst)
{
    for(unsigned int boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++)
    { scanBeBoardDacDac(boardIndex, dac1Name, dac1List, dac2Name, dac2List, numberOfEvents, detectorContainerVectorOfVector, numberOfEventsPerBurst); }

    return;
}

// Two dimensional dac scan per BeBoard
void Tool::scanBeBoardDacDac(uint16_t                                         boardIndex,
                             const std::string&                               dac1Name,
                             const std::vector<uint16_t>&                     dac1List,
                             const std::string&                               dac2Name,
                             const std::vector<uint16_t>&                     dac2List,
                             uint32_t                                         numberOfEvents,
                             std::vector<std::vector<DetectorDataContainer*>> detectorContainerVectorOfVector,
                             int32_t                                          numberOfEventsPerBurst)
{
    if(dac1List.size() != detectorContainerVectorOfVector.size())
    {
        LOG(ERROR) << __PRETTY_FUNCTION__ << " dacList and detector container vector have different sizes, aborting";
        abort();
    }

    for(size_t dacIt = 0; dacIt < dac1List.size(); ++dacIt)
    {
        // el::LoggingFlag::NewLineForContainer (0);
        if(boardIndex == 0) LOG(INFO) << BOLDBLUE << " Scanning dac1 " << dac1Name << ", value = " << dac1List[dacIt] << " vs " << dac2Name << RESET;
        // el::LoggingFlag::NewLineForContainer (1);
        setSameDacBeBoard(fDetectorContainer->at(boardIndex), dac1Name, dac1List[dacIt]);
        scanBeBoardDac(boardIndex, dac2Name, dac2List, numberOfEvents, detectorContainerVectorOfVector[dacIt], numberOfEventsPerBurst);
    }

    return;
}

// One dimensional dac scan
void Tool::scanDac(const std::string&                  dacName,
                   const std::vector<uint16_t>&        dacList,
                   uint32_t                            numberOfEvents,
                   std::vector<DetectorDataContainer*> detectorContainerVector,
                   int32_t                             numberOfEventsPerBurst)
{
    for(unsigned int boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++)
    { scanBeBoardDac(boardIndex, dacName, dacList, numberOfEvents, detectorContainerVector, numberOfEventsPerBurst); }
}

// bit wise scan
void Tool::bitWiseScan(const std::string& dacName, uint32_t numberOfEvents, const float& targetOccupancy, int32_t numberOfEventsPerBurst)
{
    for(unsigned int boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++) { bitWiseScanBeBoard(boardIndex, dacName, numberOfEvents, targetOccupancy, numberOfEventsPerBurst); }
}

// bit wise scan per BeBoard
void Tool::bitWiseScanBeBoard(uint16_t boardIndex, const std::string& dacName, uint32_t numberOfEvents, const float& targetOccupancy, int32_t numberOfEventsPerBurst)
{
    DetectorDataContainer* outputDataContainer = fDetectorDataContainer;
    ReadoutChip*           cReadoutChip        = fDetectorContainer->at(boardIndex)->at(0)->at(0)->at(0); // assumption: one BeBoard has only one type of chip;
    bool                   localDAC            = cReadoutChip->isDACLocal(dacName);
    uint8_t                numberOfBits        = cReadoutChip->getNumberOfBits(dacName);
    LOG(INFO) << BOLDBLUE << "Number of bits in this DAC is " << +numberOfBits << RESET;
    bool                   occupanyDirectlyProportionalToDAC;
    DetectorDataContainer* previousStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *previousStepOccupancyContainer);
    DetectorDataContainer* currentStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *currentStepOccupancyContainer);

    DetectorDataContainer* previousDacList = new DetectorDataContainer();
    DetectorDataContainer* currentDacList  = new DetectorDataContainer();

    uint16_t allZeroRegister = 0;
    uint16_t allOneRegister  = (0xFFFF >> (16 - numberOfBits));
    if(localDAC)
    {
        ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, *previousDacList, allZeroRegister);
        ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, *currentDacList, allOneRegister);
    }
    else
    {
        ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *previousDacList, allZeroRegister);
        ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *currentDacList, allOneRegister);
    }
    LOG(INFO) << BOLDBLUE << "Setting all bits of register " << dacName << "  to  " << +allZeroRegister << RESET;
    if(localDAC)
        setAllLocalDacBeBoard(boardIndex, dacName, *previousDacList);
    else
        setAllGlobalDacBeBoard(boardIndex, dacName, *previousDacList);

    fDetectorDataContainer = previousStepOccupancyContainer;
    LOG(INFO) << BOLDBLUE << "\t\t... measuring occupancy...." << RESET;
    measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);

    LOG(INFO) << BOLDBLUE << "Setting all bits of register " << dacName << "  to  " << +allOneRegister << RESET;
    if(localDAC)
        setAllLocalDacBeBoard(boardIndex, dacName, *currentDacList);
    else
        setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);

    fDetectorDataContainer = currentStepOccupancyContainer;
    LOG(INFO) << BOLDBLUE << "\t\t... measuring occupancy...." << RESET;
    measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);

    // This fails sometimes depending on settings, need to make it an option...
    occupanyDirectlyProportionalToDAC =
        currentStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy > previousStepOccupancyContainer->at(boardIndex)->getSummary<Occupancy, Occupancy>().fOccupancy;

    // Hacked solution for PS
    if((cReadoutChip->getFrontEndType() == FrontEndType::MPA) or (cReadoutChip->getFrontEndType() == FrontEndType::SSA) or (cReadoutChip->getFrontEndType() == FrontEndType::MPA2) or
       (cReadoutChip->getFrontEndType() == FrontEndType::SSA2))
    {
        if(localDAC)
            occupanyDirectlyProportionalToDAC = true;
        else
            occupanyDirectlyProportionalToDAC = false;
    }

    if(!occupanyDirectlyProportionalToDAC)
    {
        DetectorDataContainer* tmpPointer = previousDacList;
        previousDacList                   = currentDacList;
        currentDacList                    = tmpPointer;
    }
    // LOG (INFO) << BOLDBLUE << "START " << RESET;

    for(int iBit = numberOfBits - 1; iBit >= 0; --iBit)
    {
        LOG(INFO) << BOLDBLUE << "Bit number " << +iBit << " of " << dacName << RESET;
        for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(localDAC)
                    {
                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                        {
                            if(occupanyDirectlyProportionalToDAC)
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) =
                                    previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) + (1 << iBit);
                            else
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) =
                                    previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) &
                                    (0xFFFF - (1 << iBit));
                        }
                    }
                    else
                    {
                        if(occupanyDirectlyProportionalToDAC)
                            currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() + (1 << iBit);
                        else
                            currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() & (0xFFFF - (1 << iBit));

                        LOG(DEBUG) << BOLDBLUE << "\t.. current setting is "
                                   << currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() << RESET;
                    }
                }
            }
        }

        if(localDAC)
            setAllLocalDacBeBoard(boardIndex, dacName, *currentDacList);
        else
            setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);

        Occupancy noOccupancy;
        ContainerFactory::reinitializeContainer(currentStepOccupancyContainer, noOccupancy);
        fDetectorDataContainer = currentStepOccupancyContainer;
        measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
        // TO-DO.. generalize so that I don't need the MPA/SSA
        if(fNormalize == 0)
        {
            float cMaxOcc               = 1.0;
            auto& cDataContainerThisBrd = fDetectorDataContainer->at(boardIndex);
            for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
            {
                auto& cDataContainerThisOG = cDataContainerThisBrd->at(cOpticalGroup->getIndex());
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cDataContainerThisFE = cDataContainerThisOG->at(cHybrid->getIndex());
                    for(auto cChip: *cHybrid)
                    {
                        auto&                cDataContainerThisChip = cDataContainerThisFE->at(cChip->getIndex());
                        auto&                cSummary               = cDataContainerThisChip->getSummary<Occupancy, Occupancy>();
                        ChannelGroupHandler* cHandler;
                        if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                            cHandler = new MPAChannelGroupHandler();
                        else
                            cHandler = new SSAChannelGroupHandler();
                        LOG(DEBUG) << BOLDYELLOW << " Normalizing assuming " << fNReadbackEvents << " events and " << cHandler->allChannelGroup()->getNumberOfEnabledChannels() << " enabled channels."
                                   << RESET;
                        float  cGlobalOcc = 0;
                        size_t cNenabled  = 0;
                        for(uint16_t cChnl = 0; cChnl < cDataContainerThisChip->size(); cChnl++)
                        {
                            uint32_t cRow = cChnl % cHandler->allChannelGroup()->getNumberOfRows();
                            uint32_t cCol;
                            if(cHandler->allChannelGroup()->getNumberOfCols() == 0)
                                cCol = 0;
                            else
                                cCol = cChnl / cHandler->allChannelGroup()->getNumberOfRows();
                            if(cHandler->allChannelGroup()->isChannelEnabled(cRow, cCol))
                            {
                                cDataContainerThisChip->getChannel<Occupancy>(cRow, cCol).fOccupancy /= fNReadbackEvents;
                                cGlobalOcc += cDataContainerThisChip->getChannel<Occupancy>(cRow, cCol).fOccupancy;
                                cNenabled++;
                                if(cChnl < 10 || cChnl > 15 * 120 + 110)
                                    LOG(DEBUG) << BOLDBLUE << cChnl << " [ " << cRow << " , " << cCol << " ] " << cDataContainerThisChip->getChannel<Occupancy>(cRow, cCol).fOccupancy << RESET;
                            }
                        }
                        cGlobalOcc /= cNenabled;
                        cSummary.fOccupancy = std::min(cMaxOcc, cGlobalOcc);
                    }
                }
            }
        }

        // Determine if it is better or not
        for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    std::stringstream cOut;
                    if(localDAC)
                    {
                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                        {
                            cOut << BOLDBLUE << "localocc "
                                 << currentStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy
                                 << "\n";

                            if(currentStepOccupancyContainer->at(boardIndex)
                                   ->at(cOpticalGroup->getIndex())
                                   ->at(cHybrid->getIndex())
                                   ->at(cChip->getIndex())
                                   ->getChannel<Occupancy>(iChannel)
                                   .fOccupancy <= targetOccupancy)
                            {
                                previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) =
                                    currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel);
                                previousStepOccupancyContainer->at(boardIndex)
                                    ->at(cOpticalGroup->getIndex())
                                    ->at(cHybrid->getIndex())
                                    ->at(cChip->getIndex())
                                    ->getChannel<Occupancy>(iChannel)
                                    .fOccupancy = currentStepOccupancyContainer->at(boardIndex)
                                                      ->at(cOpticalGroup->getIndex())
                                                      ->at(cHybrid->getIndex())
                                                      ->at(cChip->getIndex())
                                                      ->getChannel<Occupancy>(iChannel)
                                                      .fOccupancy;
                            }
                        }
                    }
                    else
                    {
                        auto& cCurrentDAC = currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>();
                        cOut << "Occupancy Chip#" << +cChip->getId() << "\t[ global] "
                             << currentStepOccupancyContainer->at(boardIndex)
                                    ->at(cOpticalGroup->getIndex())
                                    ->at(cHybrid->getIndex())
                                    ->at(cChip->getIndex())
                                    ->getSummary<Occupancy, Occupancy>()
                                    .fOccupancy
                             << " for a DAC value of " << cCurrentDAC;

                        if(currentStepOccupancyContainer->at(boardIndex)
                               ->at(cOpticalGroup->getIndex())
                               ->at(cHybrid->getIndex())
                               ->at(cChip->getIndex())
                               ->getSummary<Occupancy, Occupancy>()
                               .fOccupancy <= targetOccupancy)
                        {
                            previousDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() =
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>();
                            previousStepOccupancyContainer->at(boardIndex)
                                ->at(cOpticalGroup->getIndex())
                                ->at(cHybrid->getIndex())
                                ->at(cChip->getIndex())
                                ->getSummary<Occupancy, Occupancy>()
                                .fOccupancy = currentStepOccupancyContainer->at(boardIndex)
                                                  ->at(cOpticalGroup->getIndex())
                                                  ->at(cHybrid->getIndex())
                                                  ->at(cChip->getIndex())
                                                  ->getSummary<Occupancy, Occupancy>()
                                                  .fOccupancy;
                        }
                    }
                    LOG(DEBUG) << BOLDYELLOW << cOut.str() << RESET;
                }
            }
        }
    }

    if(localDAC) { setAllLocalDacBeBoard(boardIndex, dacName, *previousDacList); }
    else
        setAllGlobalDacBeBoard(boardIndex, dacName, *previousDacList);

    fDetectorDataContainer = outputDataContainer;
    measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);

    delete previousStepOccupancyContainer;
    delete currentStepOccupancyContainer;
    delete previousDacList;
    delete currentDacList;

    return;
}

// full scan, eed a way to traport
void Tool::fullScan(const std::string& dacName, uint32_t numberOfEvents, const float& targetOccupancy, int32_t numberOfEventsPerBurst, int32_t startVal, bool mask)
{
    for(unsigned int boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++)
    { fullScanBeBoard(boardIndex, dacName, numberOfEvents, targetOccupancy, numberOfEventsPerBurst, startVal, mask); }
}

// full scan per BeBoard. Returns untrimmed objects list (channels/chips)
void Tool::fullScanBeBoard(uint16_t boardIndex, const std::string& dacName, uint32_t numberOfEvents, const float& targetOccupancy, int32_t numberOfEventsPerBurst, int32_t startVal, bool mask)
{
    DetectorDataContainer* outputDataContainer            = fDetectorDataContainer;
    ReadoutChip*           cReadoutChip                   = fDetectorContainer->at(boardIndex)->at(0)->at(0)->at(0); // assumption: one BeBoard has only one type of chip;
    bool                   localDAC                       = cReadoutChip->isDACLocal(dacName);
    DetectorDataContainer* previousStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *previousStepOccupancyContainer);
    DetectorDataContainer* currentStepOccupancyContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *currentStepOccupancyContainer);

    DetectorDataContainer* currentDacList  = new DetectorDataContainer();
    DetectorDataContainer* currentDoneList = new DetectorDataContainer();
    DetectorDataContainer* currentMaskList = new DetectorDataContainer();

    std::vector<uint32_t> returnVec;

    // For masking so we dont exclude the outliers.  only works if all chips are identical -- which is the current use case.
    std::pair<int, int>     NoccDiff(0, 0);
    std::pair<float, float> occDiff(0.0, 0.0);

    uint16_t allOneRegister = startVal;

    uint16_t              allZeroRegister  = 0;
    uint16_t              allFalseRegister = 0;
    std::vector<uint16_t> maskvec;
    if(localDAC)
    {
        ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, *currentDacList, allOneRegister);
        ContainerFactory::copyAndInitChannel<uint16_t>(*fDetectorContainer, *currentDoneList, allFalseRegister);
        ContainerFactory::copyAndInitChip<std::vector<uint16_t>>(*fDetectorContainer, *currentMaskList, maskvec);
    }
    else
    {
        ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *currentDacList, allOneRegister);
        ContainerFactory::copyAndInitChip<uint16_t>(*fDetectorContainer, *currentDoneList, allFalseRegister);
    }

    for(int iThresh = startVal; iThresh >= allZeroRegister; iThresh--)
    {
        bool first = (iThresh == startVal);

        int threshToSet = iThresh;
        if(localDAC) threshToSet = 31 - iThresh;

        returnVec.clear();

        LOG(INFO) << BOLDBLUE << "Threshold set to " << int(threshToSet) << " for " << dacName << RESET;
        for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(localDAC)
                    {
                        currentMaskList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<std::vector<uint16_t>>().clear();
                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                        {
                            if(not currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel))
                            { currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) = threshToSet; }
                        }
                    }
                    else
                    {
                        if(not currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>())
                        {
                            currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() = threshToSet;
                            LOG(DEBUG) << BOLDBLUE << "\t.. current setting is "
                                       << currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() << RESET;
                        }
                    }
                }
            }
        }

        if(localDAC)
            setAllLocalDacBeBoard(boardIndex, dacName, *currentDacList);
        else
            setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);

        Occupancy noOccupancy;
        ContainerFactory::reinitializeContainer(currentStepOccupancyContainer, noOccupancy);
        fDetectorDataContainer = currentStepOccupancyContainer;
        measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);

        // Determine if it is better or not
        for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    std::stringstream cOut;
                    if(localDAC)
                    {
                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                        {
                            cOut << BOLDBLUE << "localocc "
                                 << currentStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy
                                 << "\n";
                            if(not currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel))
                            {
                                // if (cChip->size()<500 and iChannel<10)std::cout<<"PRE occDiff
                                // "<<iChannel<<":"<<currentStepOccupancyContainer->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<Occupancy>(iChannel).fOccupancy<<std::endl;
                                currentStepOccupancyContainer->at(boardIndex)
                                    ->at(cOpticalGroup->getIndex())
                                    ->at(cHybrid->getIndex())
                                    ->at(cChip->getIndex())
                                    ->getChannel<Occupancy>(iChannel)
                                    .fOccupancy = std::max(currentStepOccupancyContainer->at(boardIndex)
                                                               ->at(cOpticalGroup->getIndex())
                                                               ->at(cHybrid->getIndex())
                                                               ->at(cChip->getIndex())
                                                               ->getChannel<Occupancy>(iChannel)
                                                               .fOccupancy,
                                                           previousStepOccupancyContainer->at(boardIndex)
                                                               ->at(cOpticalGroup->getIndex())
                                                               ->at(cHybrid->getIndex())
                                                               ->at(cChip->getIndex())
                                                               ->getChannel<Occupancy>(iChannel)
                                                               .fOccupancy);
                                if((currentStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy >= targetOccupancy) and
                                   (previousStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy < targetOccupancy) and
                                   (not first))
                                {
                                    // std::cout<<"occDiff "<<occDiff<<" NoccDiff "<<NoccDiff<<std::endl;

                                    if(std::fabs(currentStepOccupancyContainer->at(boardIndex)
                                                     ->at(cOpticalGroup->getIndex())
                                                     ->at(cHybrid->getIndex())
                                                     ->at(cChip->getIndex())
                                                     ->getChannel<Occupancy>(iChannel)
                                                     .fOccupancy -
                                                 targetOccupancy) > std::fabs(previousStepOccupancyContainer->at(boardIndex)
                                                                                  ->at(cOpticalGroup->getIndex())
                                                                                  ->at(cHybrid->getIndex())
                                                                                  ->at(cChip->getIndex())
                                                                                  ->getChannel<Occupancy>(iChannel)
                                                                                  .fOccupancy -
                                                                              targetOccupancy))
                                    {
                                        currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) -= 1;
                                        occDiff.first += previousStepOccupancyContainer->at(boardIndex)
                                                             ->at(cOpticalGroup->getIndex())
                                                             ->at(cHybrid->getIndex())
                                                             ->at(cChip->getIndex())
                                                             ->getChannel<Occupancy>(iChannel)
                                                             .fOccupancy -
                                                         targetOccupancy;

                                        NoccDiff.first += 1;
                                    }
                                    else
                                    {
                                        occDiff.second += currentStepOccupancyContainer->at(boardIndex)
                                                              ->at(cOpticalGroup->getIndex())
                                                              ->at(cHybrid->getIndex())
                                                              ->at(cChip->getIndex())
                                                              ->getChannel<Occupancy>(iChannel)
                                                              .fOccupancy -
                                                          targetOccupancy;
                                        NoccDiff.second += 1;
                                    }

                                    currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(iChannel) = 1;
                                }

                                else
                                {
                                    currentMaskList->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getSummary<std::vector<uint16_t>>()
                                        .push_back(iChannel);

                                    returnVec.push_back(iChannel);
                                    previousStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy = currentStepOccupancyContainer->at(boardIndex)
                                                          ->at(cOpticalGroup->getIndex())
                                                          ->at(cHybrid->getIndex())
                                                          ->at(cChip->getIndex())
                                                          ->getChannel<Occupancy>(iChannel)
                                                          .fOccupancy;
                                }
                            }
                        }
                    }
                    else
                    {
                        auto& cCurrentDAC = currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>();
                        cOut << "Occupancy Chip#" << +cChip->getId() << "\t[ global] "
                             << currentStepOccupancyContainer->at(boardIndex)
                                    ->at(cOpticalGroup->getIndex())
                                    ->at(cHybrid->getIndex())
                                    ->at(cChip->getIndex())
                                    ->getSummary<Occupancy, Occupancy>()
                                    .fOccupancy
                             << " for a DAC value of " << cCurrentDAC;

                        float chanavg = 0.0;
                        for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                        {
                            currentStepOccupancyContainer->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<Occupancy>(iChannel).fOccupancy =
                                std::max(currentStepOccupancyContainer->at(boardIndex)
                                             ->at(cOpticalGroup->getIndex())
                                             ->at(cHybrid->getIndex())
                                             ->at(cChip->getIndex())
                                             ->getChannel<Occupancy>(iChannel)
                                             .fOccupancy,
                                         previousStepOccupancyContainer->at(boardIndex)
                                             ->at(cOpticalGroup->getIndex())
                                             ->at(cHybrid->getIndex())
                                             ->at(cChip->getIndex())
                                             ->getChannel<Occupancy>(iChannel)
                                             .fOccupancy);

                            chanavg += float(currentStepOccupancyContainer->at(boardIndex)
                                                 ->at(cOpticalGroup->getIndex())
                                                 ->at(cHybrid->getIndex())
                                                 ->at(cChip->getIndex())
                                                 ->getChannel<Occupancy>(iChannel)
                                                 .fOccupancy >= targetOccupancy); // counts number found instead
                        }
                        chanavg /= float(cChip->size());
                        currentStepOccupancyContainer->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<Occupancy, Occupancy>().fOccupancy =
                            chanavg;

                        if(not currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>())
                        {
                            LOG(INFO) << BOLDBLUE << "currentStepOccupancyContainer "
                                      << currentStepOccupancyContainer->at(boardIndex)
                                             ->at(cOpticalGroup->getIndex())
                                             ->at(cHybrid->getIndex())
                                             ->at(cChip->getIndex())
                                             ->getSummary<Occupancy, Occupancy>()
                                             .fOccupancy
                                      << RESET;

                            if(currentStepOccupancyContainer->at(boardIndex)
                                       ->at(cOpticalGroup->getIndex())
                                       ->at(cHybrid->getIndex())
                                       ->at(cChip->getIndex())
                                       ->getSummary<Occupancy, Occupancy>()
                                       .fOccupancy >= 0.5 and
                               previousStepOccupancyContainer->at(boardIndex)
                                       ->at(cOpticalGroup->getIndex())
                                       ->at(cHybrid->getIndex())
                                       ->at(cChip->getIndex())
                                       ->getSummary<Occupancy, Occupancy>()
                                       .fOccupancy < 0.5)
                            {
                                if(std::fabs(currentStepOccupancyContainer->at(boardIndex)
                                                 ->at(cOpticalGroup->getIndex())
                                                 ->at(cHybrid->getIndex())
                                                 ->at(cChip->getIndex())
                                                 ->getSummary<Occupancy, Occupancy>()
                                                 .fOccupancy -
                                             0.5) > std::fabs(previousStepOccupancyContainer->at(boardIndex)
                                                                  ->at(cOpticalGroup->getIndex())
                                                                  ->at(cHybrid->getIndex())
                                                                  ->at(cChip->getIndex())
                                                                  ->getSummary<Occupancy, Occupancy>()
                                                                  .fOccupancy -
                                                              0.5))
                                { currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() += 1; }
                                currentDoneList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>() = 1;
                            }
                            else
                            {
                                returnVec.push_back(cChip->getIndex());

                                previousStepOccupancyContainer->at(boardIndex)
                                    ->at(cOpticalGroup->getIndex())
                                    ->at(cHybrid->getIndex())
                                    ->at(cChip->getIndex())
                                    ->getSummary<Occupancy, Occupancy>()
                                    .fOccupancy = currentStepOccupancyContainer->at(boardIndex)
                                                      ->at(cOpticalGroup->getIndex())
                                                      ->at(cHybrid->getIndex())
                                                      ->at(cChip->getIndex())
                                                      ->getSummary<Occupancy, Occupancy>()
                                                      .fOccupancy;

                                for(uint32_t iChannel = 0; iChannel < cChip->size(); ++iChannel)
                                {
                                    previousStepOccupancyContainer->at(boardIndex)
                                        ->at(cOpticalGroup->getIndex())
                                        ->at(cHybrid->getIndex())
                                        ->at(cChip->getIndex())
                                        ->getChannel<Occupancy>(iChannel)
                                        .fOccupancy = currentStepOccupancyContainer->at(boardIndex)
                                                          ->at(cOpticalGroup->getIndex())
                                                          ->at(cHybrid->getIndex())
                                                          ->at(cChip->getIndex())
                                                          ->getChannel<Occupancy>(iChannel)
                                                          .fOccupancy;
                                }
                            }
                        }
                    }
                    LOG(DEBUG) << BOLDYELLOW << cOut.str() << RESET;
                }
            }
        }
        size_t nRunning = returnVec.size();
        LOG(INFO) << BOLDYELLOW << "Pedestals remaining: " << nRunning << RESET;
        if(nRunning == 0) break;
    }
    fDetectorDataContainer = outputDataContainer;
    measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
    if(localDAC)
    {
        occDiff.first  = (occDiff.first) / float(NoccDiff.first);
        occDiff.second = (occDiff.second) / float(NoccDiff.second);

        // Turn off for now -- todo
        occDiff.first  = 0;
        occDiff.second = 0;
        // Turn off for now -- todo

        // LOG(INFO) << BOLDYELLOW << "maxDiff:  " <<maxDiff<<RESET;

        for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    auto cOriginalMask = cChip->getChipOriginalMask();
                    if(localDAC)
                    {
                        std::vector<uint16_t> tempMaskList =
                            currentMaskList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<std::vector<uint16_t>>();

                        for(uint32_t iChannel = 0; iChannel < tempMaskList.size(); ++iChannel)
                        {
                            // LOG(INFO) << BOLDYELLOW << "occDiff.first  "<< occDiff.first << " occDiff.second  "<< occDiff.second  <<RESET;
                            // LOG(INFO) << BOLDYELLOW << "SUM.first  "<< occDiff.first+targetOccupancy << " SUM.second  "<< occDiff.second +targetOccupancy <<RESET;

                            if(currentStepOccupancyContainer->at(boardIndex)
                                   ->at(cOpticalGroup->getIndex())
                                   ->at(cHybrid->getIndex())
                                   ->at(cChip->getIndex())
                                   ->getChannel<Occupancy>(tempMaskList[iChannel])
                                   .fOccupancy > targetOccupancy)
                            {
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(tempMaskList[iChannel]) = 0;

                                if(mask)
                                {
                                    if((currentStepOccupancyContainer->at(boardIndex)
                                            ->at(cOpticalGroup->getIndex())
                                            ->at(cHybrid->getIndex())
                                            ->at(cChip->getIndex())
                                            ->getChannel<Occupancy>(tempMaskList[iChannel])
                                            .fOccupancy) > (targetOccupancy + 2.0 * occDiff.second))
                                    {
                                        cOriginalMask->disableChannel(tempMaskList[iChannel]);
                                        LOG(INFO) << BOLDRED << "Masking Channel:  " << tempMaskList[iChannel] << RESET;
                                        // LOG(INFO) << BOLDYELLOW << "MASKHIGH"<<RESET;
                                    }
                                }
                            }
                            else
                            {
                                currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(tempMaskList[iChannel]) = 31;
                                if(mask)
                                {
                                    if((currentStepOccupancyContainer->at(boardIndex)
                                            ->at(cOpticalGroup->getIndex())
                                            ->at(cHybrid->getIndex())
                                            ->at(cChip->getIndex())
                                            ->getChannel<Occupancy>(tempMaskList[iChannel])
                                            .fOccupancy) < (targetOccupancy + 2.0 * occDiff.first))
                                    {
                                        cOriginalMask->disableChannel(tempMaskList[iChannel]);
                                        LOG(INFO) << BOLDRED << "Masking Channel:  " << tempMaskList[iChannel] << RESET;
                                        // LOG(INFO) << BOLDYELLOW << "MASKLOW"<<RESET;
                                    }
                                }
                            }
                            // LOG(INFO) << BOLDYELLOW << "DACL:  "<<
                            // currentDacList->at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<uint16_t>(returnVec[iChannel])<<RESET;
                        }
                    }
                    // else -> probably wont be masking chips
                    // fReadoutChipInterface->ConfigureChipOriginalMask(cChip);
                    fReadoutChipInterface->maskChannelGroup(cChip, cOriginalMask);
                }
            }
        }
    }
    if(localDAC)
        setAllLocalDacBeBoard(boardIndex, dacName, *currentDacList);
    else
        setAllGlobalDacBeBoard(boardIndex, dacName, *currentDacList);

    delete previousStepOccupancyContainer;
    delete currentStepOccupancyContainer;
    delete currentDacList;
}

// set dac and measure occupancy
void Tool::setDacAndMeasureData(const std::string& dacName, const uint16_t dacValue, uint32_t numberOfEvents, int32_t numberOfEventsPerBurst)
{
    for(uint16_t boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++) { setDacAndMeasureBeBoardData(boardIndex, dacName, dacValue, numberOfEvents, numberOfEventsPerBurst); }
}

// Set dac and measure occupancy per BeBoard
void Tool::setDacAndMeasureBeBoardData(uint16_t boardIndex, const std::string& dacName, const uint16_t dacValue, uint32_t numberOfEvents, int32_t numberOfEventsPerBurst)
{
    setSameDacBeBoard(fDetectorContainer->at(boardIndex), dacName, dacValue);
    measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
}

// Measure occupancy
void Tool::measureData(uint32_t numberOfEvents, int32_t numberOfEventsPerBurst)
{
    for(unsigned int boardIndex = 0; boardIndex < fDetectorContainer->size(); boardIndex++) measureBeBoardData(boardIndex, numberOfEvents, numberOfEventsPerBurst);
}

class ScanBase
{
  public:
    ScanBase(Tool* theTool) : fTool(theTool) { ; }
    virtual ~ScanBase() { ; }

    virtual void operator()() = 0;
    void         setGroupHandlerContainer(const DetectorDataContainer* theChannelHandlerContainer, bool isSameChannelGroupForAllChannels)
    {
        fChannelHandlerContainer        = theChannelHandlerContainer;
        fSameChannelGroupForAllChannels = isSameChannelGroupForAllChannels;
    }
    void setGroup(int groupNumber) { fGroupNumber = groupNumber; }
    // void         setGroup(const ChannelGroupBase* cTestChannelGroup) { fTestChannelGroup = cTestChannelGroup; }
    void setBoardId(uint16_t boardIndex) { fBoardIndex = boardIndex; }
    void setNumberOfEvents(uint32_t numberOfEvents) { fNumberOfEvents = numberOfEvents; }
    void setNumberOfEventsPerBurst(int32_t numberOfEventsPerBurst) { fNumberOfEventsPerBurst = numberOfEventsPerBurst; }

    void setDetectorContainer(DetectorContainer* detectorContainer) { fDetectorContainer = detectorContainer; }

  protected:
    uint32_t                     fNumberOfEvents;
    int32_t                      fNumberOfEventsPerBurst{-1};
    uint32_t                     fNumberOfMSec;
    uint32_t                     fBoardIndex;
    const DetectorDataContainer* fChannelHandlerContainer;
    uint                         fGroupNumber;
    Tool*                        fTool;
    DetectorContainer*           fDetectorContainer;
    bool                         fSameChannelGroupForAllChannels;

    inline const std::shared_ptr<ChannelGroupBase> getChannelGroup(int groupNumber, uint16_t boardId, uint16_t opticalGroupId, uint16_t hybridId, uint16_t chipId)
    {
        return fChannelHandlerContainer->getObject(boardId)
            ->getObject(opticalGroupId)
            ->getObject(hybridId)
            ->getObject(chipId)
            ->getSummary<std::shared_ptr<ChannelGroupHandler>>()
            ->getTestGroup(groupNumber);
    }

    inline const std::shared_ptr<ChannelGroupBase> getChannelGroup(int groupNumber)
    {
        // LOG (INFO) << BOLDYELLOW << "Get channel group ScanBase group#" << groupNumber << RESET;

        return fChannelHandlerContainer->at(0)->at(0)->at(0)->at(0)->getSummary<std::shared_ptr<ChannelGroupHandler>>()->getTestGroup(groupNumber);
    }
};

void Tool::doScanOnAllGroupsBeBoard(uint16_t boardIndex, uint32_t numberOfEvents, int32_t numberOfEventsPerBurst, ScanBase* groupScan)
{
    groupScan->setBoardId(boardIndex);
    groupScan->setNumberOfEvents(numberOfEvents);
    groupScan->setDetectorContainer(fDetectorContainer);
    groupScan->setNumberOfEventsPerBurst(numberOfEventsPerBurst);
    groupScan->setGroupHandlerContainer(getChannelGroupHandlerContainer(), fSameChannelGroupForAllChannels);
    if(!fAllChan)
    {
        uint16_t maxNumberOfGroups = getMaxNumberOfGroups();
        for(uint16_t groupNumber = 0; groupNumber < maxNumberOfGroups; ++groupNumber)
        {
            if(fMaskChannelsFromOtherGroups || fTestPulse)
            {
                for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            if(groupNumber > getChannelGroupHandlerContainer()
                                                 ->getObject(fDetectorContainer->getObject(boardIndex)->getId())
                                                 ->getObject(cOpticalGroup->getId())
                                                 ->getObject(cHybrid->getId())
                                                 ->getObject(cChip->getId())
                                                 ->getSummary<std::shared_ptr<ChannelGroupHandler>>()
                                                 ->getNumberOfGroups())
                                continue;
                            fReadoutChipInterface->maskChannelsAndSetInjectionSchema(cChip,
                                                                                     getChannelGroupHandlerContainer()
                                                                                         ->getObject(fDetectorContainer->at(boardIndex)->getId())
                                                                                         ->getObject(cOpticalGroup->getId())
                                                                                         ->getObject(cHybrid->getId())
                                                                                         ->getObject(cChip->getId())
                                                                                         ->getSummary<std::shared_ptr<ChannelGroupHandler>>()
                                                                                         ->getTestGroup(groupNumber),
                                                                                     fMaskChannelsFromOtherGroups,
                                                                                     fTestPulse);
                        }
                    }
                }
            }
            groupScan->setGroup(groupNumber);
            (*groupScan)();
            // this->sendData();
        }

        if(fMaskChannelsFromOtherGroups) // Re-enable all the channels and evaluate
        {
            for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid) { fReadoutChipInterface->ConfigureChipOriginalMask(cChip); }
                }
            }
        }
    }
    else
    {
        groupScan->setGroup(-1);
        (*groupScan)();
    }
}

class MeasureBeBoardDataPerGroup : public ScanBase
{
  public:
    MeasureBeBoardDataPerGroup(Tool* theTool) : ScanBase(theTool) { ; }
    ~MeasureBeBoardDataPerGroup() { ; }

    void operator()() override
    {
        uint32_t burstNumbers;
        uint32_t lastBurstNumberOfEvents;
        if(fNumberOfEventsPerBurst <= 0)
        {
            burstNumbers            = 1;
            lastBurstNumberOfEvents = fNumberOfEvents;
        }
        else
        {
            burstNumbers            = fNumberOfEvents / fNumberOfEventsPerBurst;
            lastBurstNumberOfEvents = fNumberOfEventsPerBurst;
            if(fNumberOfEvents % fNumberOfEventsPerBurst > 0)
            {
                ++burstNumbers;
                lastBurstNumberOfEvents = fNumberOfEvents % fNumberOfEventsPerBurst;
            }
        }

        while(burstNumbers > 0)
        {
            uint32_t currentNumberOfEvents = uint32_t(fNumberOfEventsPerBurst);
            if(burstNumbers == 1) currentNumberOfEvents = lastBurstNumberOfEvents;
            // LOG (INFO) << BOLDYELLOW << "Tool::ReadNEvents : number of events requested is " << +currentNumberOfEvents << RESET;
            if(fTool->ifUseReadNEvents())
                fTool->ReadNEvents(fDetectorContainer->at(fBoardIndex), currentNumberOfEvents);
            else
            {
                LOG(INFO) << BOLDYELLOW << "Will use measureBeBoardData with ReadData " << RESET;
                fTool->fBeBoardInterface->Start(fDetectorContainer->at(fBoardIndex));
                std::this_thread::sleep_for(std::chrono::milliseconds(fTool->getWait()));
                fTool->fBeBoardInterface->Stop(fDetectorContainer->at(fBoardIndex));
                fTool->ReadData(fDetectorContainer->at(fBoardIndex), false);
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }
            // Loop over Events from this Acquisition
            const std::vector<Event*>& events = fTool->GetEvents();
            fTool->setNReadbackEvents(events.size());
            // Assuming all chip will have all channels enabled:
            if(fSameChannelGroupForAllChannels)
            {
                // LOG (INFO) << BOLDYELLOW << "MeasureBeBoardDataPerGroup fSameChannelGroupForAllChannels read-back " << events.size() << " event." << RESET;
                auto channelGroup = this->getChannelGroup(fGroupNumber);
                if(channelGroup == nullptr)
                    LOG(ERROR) << BOLDRED << "Channel group does not exist..." << RESET;
                else
                {
                    for(auto& event: events) event->fillDataContainer(fDetectorDataContainer->at(fBoardIndex), channelGroup);
                }
            }
            else
            {
                LOG(DEBUG) << BOLDYELLOW << "MeasureBeBoardDataPerGroup !fSameChannelGroupForAllChannels read-back " << events.size() << RESET;
                for(auto cOpticalGroup: *fDetectorDataContainer->at(fBoardIndex))
                {
                    for(const auto cHybrid: *cOpticalGroup)
                    {
                        for(const auto cChip: *cHybrid)
                        {
                            auto channelGroup = this->getChannelGroup(fGroupNumber, fDetectorDataContainer->at(fBoardIndex)->getId(), cOpticalGroup->getId(), cHybrid->getId(), cChip->getId());
                            for(auto& event: events) event->fillChipDataContainer(cChip, channelGroup, cHybrid->getId());
                        }
                    }
                }
            }
            --burstNumbers;
        }
    }

    void setDataContainer(DetectorDataContainer* detectorDataContainer) { fDetectorDataContainer = detectorDataContainer; }

  private:
    DetectorDataContainer* fDetectorDataContainer;
};

void Tool::measureBeBoardData(uint16_t boardIndex, uint32_t numberOfEvents, int32_t numberOfEventsPerBurst)
{
    MeasureBeBoardDataPerGroup theScan(this);
    theScan.setDataContainer(fDetectorDataContainer);
    // make sure async mode uses ReadNEvents
    bool cUseReadNEvents = fUseReadNEvents;
    if(fDetectorContainer->at(boardIndex)->getEventType() == EventType::PSAS)
    {
        this->setSameGlobalDac("AnalogueAsync", 1);
        //#FIXME the commented block bellow throws "virtual bool Ph2_HwInterface::ReadoutChipInterface::maskChannelGroup(Ph2_HwDescription::ReadoutChip*, std::shared_ptr<ChannelGroupBase>, bool)
        // Error: implementation of virtual member function is absent"
        /*
                for(auto cBoard: *fDetectorContainer)
                {
                    for(auto cOpticalGroup: *cBoard)
                    {
                        for(auto cHybrid: *cOpticalGroup)
                        {
                            for(auto cChip: *cHybrid) { fReadoutChipInterface->maskChannelGroup(cChip, cChip->getChipOriginalMask()); }
                        }
                    }
                }
        */
        fUseReadNEvents = true;
    }
    doScanOnAllGroupsBeBoard(boardIndex, numberOfEvents, numberOfEventsPerBurst, &theScan);

    // If in async mode normalization is a little different ..
    // normalize by the number of triggers to accept
    // if(fDetectorContainer->at(boardIndex)->getEventType() == EventType::PSAS)
    // {
    //     numberOfEvents = fBeBoardInterface->ReadBoardReg(fDetectorContainer->at(boardIndex), "fc7_daq_stat.fast_command_block.trigger_in_counter");
    //     // LOG (INFO) << BOLDYELLOW << "Tool::measureBeBoardData number of events with PSAS " << numberOfEvents << RESET;
    //     fNReadbackEvents = numberOfEvents;
    // }

    if(fDetectorContainer->at(boardIndex)->getBoardType() == BoardType::D19C)
    { numberOfEvents = numberOfEvents * (fBeBoardInterface->ReadBoardReg(fDetectorContainer->at(boardIndex), "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity") + 1); }
    if(!fUseReadNEvents) numberOfEvents = fNReadbackEvents;

    if(fNormalize)
    {
        auto cTmp = fDetectorDataContainer->at(boardIndex)->normalizeAndAverageContainers(fDetectorContainer->at(boardIndex), getChannelGroupHandlerContainer()->at(boardIndex), numberOfEvents);
        LOG(DEBUG) << BOLDYELLOW << __PRETTY_FUNCTION__ << cTmp << RESET;
    }
    fUseReadNEvents = cUseReadNEvents;
}

class ScanBeBoardDacPerGroup : public MeasureBeBoardDataPerGroup
{
  public:
    ScanBeBoardDacPerGroup(Tool* theTool) : MeasureBeBoardDataPerGroup(theTool) { ; }
    ~ScanBeBoardDacPerGroup() { ; }

    void operator()() override
    {
        for(size_t dacIt = 0; dacIt < fDacList->size(); ++dacIt)
        {
            fTool->setSameDacBeBoard(static_cast<BeBoard*>(fDetectorContainer->at(fBoardIndex)), fDacName, fDacList->at(dacIt));
            setDataContainer(fDetectorDataContainerVector->at(dacIt));
            MeasureBeBoardDataPerGroup::operator()();
        }
    }

    void setDataContainerVector(std::vector<DetectorDataContainer*>* detectorDataContainerVector) { fDetectorDataContainerVector = detectorDataContainerVector; }
    void setDacName(const std::string& dacName) { fDacName = dacName; }
    void setDacList(const std::vector<uint16_t>* dacList) { fDacList = dacList; }

  private:
    std::vector<DetectorDataContainer*>* fDetectorDataContainerVector;
    const std::vector<uint16_t>*         fDacList;
    std::string                          fDacName;
};

// One dimensional dac scan per BeBoard
void Tool::scanBeBoardDac(uint16_t                             boardIndex,
                          const std::string&                   dacName,
                          const std::vector<uint16_t>&         dacList,
                          uint32_t                             numberOfEvents,
                          std::vector<DetectorDataContainer*>& detectorContainerVector,
                          int32_t                              numberOfEventsPerBurst)
{
    if(dacList.size() != detectorContainerVector.size())
    {
        LOG(ERROR) << __PRETTY_FUNCTION__ << " dacList and detector container vector have different sizes, aborting";
        abort();
    }

    if(RD53Shared::firstChip->getFrontEndType() == FrontEndType::RD53A)
    {
        // #######################
        // # Loop over DAC ...   #
        // # Loop over goups ... #
        // #######################

        for(size_t dacIt = 0; dacIt < dacList.size(); ++dacIt)
        {
            fDetectorDataContainer = detectorContainerVector[dacIt];
            setDacAndMeasureBeBoardData(boardIndex, dacName, dacList[dacIt], numberOfEvents, numberOfEventsPerBurst);
            this->sendData();
        }
    }
    else
    {
        // #######################
        // # Loop over goups ... #
        // # Loop over DAC ...   #
        // #######################

        ScanBeBoardDacPerGroup theScan(this);
        theScan.setDataContainerVector(&detectorContainerVector);
        theScan.setDacName(dacName);
        theScan.setDacList(&dacList);

        doScanOnAllGroupsBeBoard(boardIndex, numberOfEvents, numberOfEventsPerBurst, &theScan);
        if(fDetectorContainer->at(boardIndex)->getBoardType() == BoardType::D19C)
        { numberOfEvents = numberOfEvents * (fBeBoardInterface->ReadBoardReg(fDetectorContainer->at(boardIndex), "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity") + 1); }
        for(auto container: detectorContainerVector) container->normalizeAndAverageContainers(fDetectorContainer, getChannelGroupHandlerContainer(), numberOfEvents);
    }
}

// Set global DAC for all CBCs in the BeBoard
void Tool::setAllGlobalDacBeBoard(uint16_t boardIndex, const std::string& dacName, DetectorDataContainer& globalDACContainer)
{
    for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                fReadoutChipInterface->WriteChipReg(
                    cChip, dacName, globalDACContainer.at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getSummary<uint16_t>());
            }
        }
    }
}

// Set local dac per BeBoard
void Tool::setAllLocalDacBeBoard(uint16_t boardIndex, const std::string& dacName, DetectorDataContainer& globalDACContainer)
{
    for(auto cOpticalGroup: *(fDetectorContainer->at(boardIndex)))
        for(auto cHybrid: *cOpticalGroup)
            for(auto cChip: *cHybrid)
                fReadoutChipInterface->WriteChipAllLocalReg(cChip, dacName, *globalDACContainer.at(boardIndex)->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex()));
}

// Set same global DAC for all chips
void Tool::setSameGlobalDac(const std::string& dacName, const uint16_t dacValue)
{
    for(auto cBoard: *fDetectorContainer) setSameGlobalDacBeBoard(static_cast<BeBoard*>(cBoard), dacName, dacValue);
}

// Set same global DAC for all chips in the BeBoard
void Tool::setSameGlobalDacBeBoard(BeBoard* pBoard, const std::string& dacName, const uint16_t dacValue)
{
    if(fDoBoardBroadcast == false)
        for(auto cOpticalGroup: *pBoard)
            for(auto cHybrid: *cOpticalGroup)
                if(fDoHybridBroadcast == false)
                    for(auto cChip: *cHybrid) fReadoutChipInterface->WriteChipReg(static_cast<ReadoutChip*>(cChip), dacName, dacValue);
                else
                    fReadoutChipInterface->WriteHybridBroadcastChipReg(static_cast<Hybrid*>(cHybrid), dacName, dacValue);
    else
        fReadoutChipInterface->WriteBoardBroadcastChipReg(pBoard, dacName, dacValue);
}

// Set same local dac for all BeBoard
void Tool::setSameLocalDac(const std::string& dacName, const uint16_t dacValue)
{
    LOG(INFO) << BOLDMAGENTA << "Setting local dac [ " << dacName << " ] to " << dacValue << RESET;
    for(auto cBoard: *fDetectorContainer) { setSameLocalDacBeBoard(static_cast<BeBoard*>(cBoard), dacName, dacValue); }

    return;
}

// Set same local dac per BeBoard
void Tool::setSameLocalDacBeBoard(BeBoard* pBoard, const std::string& dacName, const uint16_t dacValue)
{
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                ChannelContainer<uint16_t>* dacVector = new ChannelContainer<uint16_t>(cChip->getNumberOfChannels(), dacValue);
                ChipContainer               theChipContainer(cChip->getIndex(), cChip->getNumberOfRows(), cChip->getNumberOfCols());
                theChipContainer.setChannelContainer(dacVector);

                fReadoutChipInterface->WriteChipAllLocalReg(cChip, dacName, theChipContainer);
            }
        }
    }
}

void Tool::setSameDacBeBoard(BeBoard* pBoard, const std::string& dacName, const uint16_t dacValue)
{
    // Assumption: 1 BeBoard has only 1 chip flavor
    if(static_cast<ReadoutChip*>(pBoard->at(0)->at(0)->at(0))->isDACLocal(dacName)) { setSameLocalDacBeBoard(pBoard, dacName, dacValue); }
    else
    {
        setSameGlobalDacBeBoard(pBoard, dacName, dacValue);
    }
}

void Tool::setSameDac(const std::string& dacName, const uint16_t dacValue)
{
    for(auto cBoard: *fDetectorContainer) { setSameDacBeBoard(static_cast<BeBoard*>(cBoard), dacName, dacValue); }
}

std::string Tool::getCalibrationName(void)
{
    int32_t     status;
    std::string className     = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
    std::string emptyTemplate = "<> ";
    size_t      found         = className.find(emptyTemplate);

    while(found != std::string::npos)
    {
        className.erase(found, emptyTemplate.length());
        found = className.find(emptyTemplate);
    }

    return className;
}

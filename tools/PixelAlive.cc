#include "tools/PixelAlive.h"
#include "HWDescription/Cbc.h"
#include "HWDescription/SSA.h"
#include "HWInterface/D19cFWInterface.h"
#include "Utils/CBCChannelGroupHandler.h"
#include "Utils/Container.h"
#include "Utils/ContainerFactory.h"
#include "Utils/ContainerStream.h"
#include "Utils/EmptyContainer.h"
#include "Utils/MPAChannelGroupHandler.h"
#include "Utils/Occupancy.h"
#include "Utils/SSAChannelGroupHandler.h"
#include "Utils/ThresholdAndNoise.h"
#include "boost/format.hpp"
#include <math.h>

#ifdef __USE_ROOT__
// static_assert(false,"use root is defined");
#include "DQMUtils/DQMHistogramPixelAlive.h"
#endif

PixelAlive::PixelAlive() : Tool() {}

PixelAlive::~PixelAlive() { clearDataMembers(); }

void PixelAlive::cleanContainerVector()
{
//    for(auto container: fSCurveStripOccupancyMap) fRecycleBin.free(container.second);
//    for(auto container: fSCurvePixelOccupancyMap) fRecycleBin.free(container.second);
//    fSCurveStripOccupancyMap.clear();
//    fSCurvePixelOccupancyMap.clear();
}

void PixelAlive::clearDataMembers()
{
    return;
    // delete fBoardRegContainer;
//    delete fThresholdAndNoiseContainer;
    if(fDisableStubLogic)
    {
        delete fStubLogicValue;
        delete fHIPCountValue;
    }
    cleanContainerVector();
}

//Does Setup before any data taking
void PixelAlive::Initialise()
{
    for(auto cBoard: *fDetectorContainer)
    {
        BeBoardRegMap cRegMap      = cBoard->getBeBoardRegMap();
        uint32_t      cTriggerFreq = cRegMap["fc7_daq_cnfg.fast_command_block.user_trigger_frequency"];

        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.clear();
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.user_trigger_frequency", cTriggerFreq});
        cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
//        LOG(INFO) << BOLDYELLOW << "Noise measured on BeBoard#" << +cBoard->getId() << " with a trigger rate of " << cTriggerFreq << "kHz." << RESET;
    }
    fDisableStubLogic = false;



    this->enableTestPulse(true); //For Testing Purposes

    fWithSSA = false;
    fWithMPA = false;
    std::vector<FrontEndType> cAllFrontEndTypes;
    for(auto cBoard: *fDetectorContainer)
    {
        auto cFrontEndTypes = cBoard->connectedFrontEndTypes();
        fWithSSA            = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA) != cFrontEndTypes.end() ||
                   std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA2) != cFrontEndTypes.end();
        fWithMPA = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::MPA) != cFrontEndTypes.end() ||
                   std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::MPA2) != cFrontEndTypes.end();
        for(auto cFrontEndType: cFrontEndTypes)
        {
            if(std::find(cAllFrontEndTypes.begin(), cAllFrontEndTypes.end(), cFrontEndType) == cAllFrontEndTypes.end()) cAllFrontEndTypes.push_back(cFrontEndType);
        }
    }
    if(fWithSSA && !fWithMPA) LOG(INFO) << BOLDBLUE << "PixelAlive with SSAs" << RESET;
    if(fWithMPA && !fWithSSA) LOG(INFO) << BOLDBLUE << "PixelAlive with MPAs" << RESET;
    if(fWithSSA && fWithMPA) LOG(INFO) << BOLDBLUE << "PixelAlive with SSAs+MPAs" << RESET;

    for(auto cFrontEndType: cAllFrontEndTypes)
    {
        if(cFrontEndType == FrontEndType::SSA || cFrontEndType == FrontEndType::SSA2)
        {
            SSAChannelGroupHandler theChannelGroupHandler;
            theChannelGroupHandler.setChannelGroupParameters(1, NSSACHANNELS); // 16*2*8
            setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
        }
        else if(cFrontEndType == FrontEndType::MPA || cFrontEndType == FrontEndType::MPA2)
        {
            MPAChannelGroupHandler theChannelGroupHandler;
            theChannelGroupHandler.setChannelGroupParameters(1, NSSACHANNELS * NMPACOLS); // 16*2*8
            setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
        }
    }

    initializeRecycleBin();

//    fAllChan = pAllChan;

	//Reads in options from xml file, has corresponding stuff in header, will need this later
    fSkipMaskedChannels          = findValueInSettings<double>("SkipMaskedChannels", 0); //after comma is default
    
    //Pedenoise parameters
    /*
    fMaskChannelsFromOtherGroups = findValueInSettings<double>("MaskChannelsFromOtherGroups", 1);
    fPlotSCurves                 = findValueInSettings<double>("PlotSCurves", 0);
    fFitSCurves                  = findValueInSettings<double>("FitSCurves", 0);
    std::cout << +fPulseAmplitudePix << std::endl;
    fPedeNoiseLimit          = findValueInSettings<double>("PedeNoiseLimit", 10);
    fPedeNoiseMask           = findValueInSettings<double>("PedeNoiseMask", 0);
    fPedeNoiseMaskUntrimmed  = findValueInSettings<double>("PedeNoiseMaskUntrimmed", 0);
    fPedeNoiseUntrimmedLimit = findValueInSettings<double>("PedeNoiseUntrimmedLimit", 0.0);
    fUseFixRange             = findValueInSettings<double>("PedeNoiseUseFixRange", 0);
    fMinThreshold            = findValueInSettings<double>("PedeNoiseMinThreshold", 0);
    fMaxThreshold            = findValueInSettings<double>("PedeNoiseMaxThreshold", 0);
    fMaskingThreshold        = findValueInSettings<double>("MaskingThreshold", 0.001);
    */
    fEventsPerPoint          = findValueInSettings<double>("Nevents", 10);
    fNEventsPerBurst = (fEventsPerPoint >= fMaxNevents) ? fMaxNevents : fEventsPerPoint;
    fNeventsForValidation    = findValueInSettings<double>("NeventsForValidation", 10000);
    LOG(INFO) << "Parsed settings:";
    LOG(INFO) << " Nevents = " << fEventsPerPoint;


    fPulseAmplitude              = findValueInSettings<double>("PedeNoisePulseAmplitude", 0);
    fPulseAmplitudePix           = findValueInSettings<double>("PedeNoisePulseAmplitudePix", fPulseAmplitude);
    // LOG(INFO) << " Fast Counter Readout [PS] " << +cEnableFastCounterReadout << RESET;

	//Saves original board config, so set back at the end
    ContainerFactory::copyAndInitBoard<BeBoardRegMap>(*fDetectorContainer, fBoardRegContainer);
    for(auto cBoard: *fDetectorContainer)
    {
        auto&                cBoardRegNap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        const BeBoardRegMap& cOrigRegMap  = static_cast<const BeBoard*>(cBoard)->getBeBoardRegMap();
        cBoardRegNap.insert(cOrigRegMap.begin(), cOrigRegMap.end());
    }

    // make sure register tracking is on
    for(auto board: *fDetectorContainer)
    {
        for(auto opticalGroup: *board)
        {
            for(auto hybrid: *opticalGroup)
            {
                for(auto chip: *hybrid)
                {
                    chip->setRegisterTracking(1);
                    chip->ClearModifiedRegisterMap();
                }
            }
        }
    }

    // for now.. force to use async mode here (does not track timing of hits), only used for testing
    bool cForcePSasync = true;
    // event types
    fEventTypes.clear();
    for(auto cBoard: *fDetectorContainer)
    {
        fEventTypes.push_back(cBoard->getEventType());
        if(!fWithSSA && !fWithMPA) continue;
        if(!cForcePSasync) continue;
        cBoard->setEventType(EventType::PSAS); //Sets up board to expect async input
        static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface())->InitializePSCounterFWInterface(cBoard);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1); } //Tells chip to expect async
            }
        }
    }

//Once I have the histogram code working add this back in
#ifdef __USE_ROOT__
    LOG(INFO) << BOLDYELLOW << "PixelAlive Book" << RESET;
    fDQMHistogramPixelAlive.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}

//Resets all registers saved in initialize, will need to look this over
void PixelAlive::Reset()
{
    LOG(INFO) << BOLDGREEN << "Resetting registers touched  by PixelAlive" << RESET;
    // set everything back to original values .. like I wasn't here
    for(auto cBoard: *fDetectorContainer)
    {
        BeBoard* theBoard = static_cast<BeBoard*>(cBoard);
        LOG(INFO) << BOLDBLUE << "Resetting all registers on back-end board " << +cBoard->getId() << RESET;
        auto&                                         cBeRegMap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        std::vector<std::pair<std::string, uint32_t>> cVecBeBoardRegs;
        cVecBeBoardRegs.clear();
        for(auto cReg: cBeRegMap) { cVecBeBoardRegs.push_back(make_pair(cReg.first, cReg.second)); }
        fBeBoardInterface->WriteBoardMultReg(theBoard, cVecBeBoardRegs);

        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                LOG(DEBUG) << BOLDBLUE << "PixelAlive::Resetting all registers on readout chips connected to FEhybrid#" << +(cHybrid->getId()) << " back to their original values..." << RESET;

                for(auto cChip: *cHybrid)
                {
                    auto cModMap = cChip->GetModifiedRegisterMap();
                    LOG(DEBUG) << BOLDYELLOW << "Chip#" << +cChip->getId() << " map of modified registers contains " << cModMap.size() << " items." << RESET;
                    std::vector<std::pair<std::string, uint16_t>> cRegList;
                    for(auto cMapItem: cModMap)
                    {
                        auto cValueInMemory = cChip->getReg(cMapItem.first);
                        if(cMapItem.second.fValue == cValueInMemory) continue;
                        // don't reconfigure the offsets .. whole point of this excercise
                        if(cMapItem.first.find("ENFLAGS") != std::string::npos) { cMapItem.second.fValue = (cMapItem.second.fValue & 0xfe) + (cValueInMemory & 0x1); }; //This conserves pixel masking
                        LOG(DEBUG) << BOLDYELLOW << "PedestalEqualization::Resetting Register " << cMapItem.first << " on Chip#" << +cChip->getId() << " from " << cValueInMemory << " to "
                                   << cMapItem.second.fValue << RESET;
                        cRegList.push_back(std::make_pair(cMapItem.first, cMapItem.second.fValue));
                    }
                    fReadoutChipInterface->WriteChipMultReg(cChip, cRegList, false);
                    // don't track registers + clear mod reg map
                    cChip->setRegisterTracking(0);
                    cChip->ClearModifiedRegisterMap();
                }
            }
        }
    }
    resetPointers();
}

//Print Out Results and runs functions
void PixelAlive::measurePixels()
{
    LOG(INFO) << BOLDBLUE << "Measure Pixel Occupancy" << RESET;
    measureOccupancy();
//Commenting out plotting for now
//    LOG(INFO) << BOLDBLUE << "Produce PixelAlive Plots" << RESET;
//    producePixelAlivePlots();
    LOG(INFO) << BOLDBLUE << "Done" << RESET;

}



//Make own version of plotmaker
void PixelAlive::producePixelAlivePlots()
{
#ifdef __USE_ROOT__
//    fDQMHistogramPixelAlive.fillPedestalAndNoisePlots(*fThresholdAndNoiseContainer);


#endif
}


//Make own version of channel masking
void PixelAlive::maskNoisyChannels(BoardDataContainer* board)
{
/*
    for(auto opticalGroup: *board)
    {
        for(auto hybrid: *opticalGroup)
        {
            for(auto chip: *hybrid)
            {
                LOG(INFO) << BOLDYELLOW << chip->getId() << RESET;
                auto chipDC = fDetectorContainer->at(board->getIndex())->at(opticalGroup->getIndex())->at(hybrid->getIndex())->at(chip->getIndex());
                // auto cType = chipDC->getFrontEndType();

                // uint32_t       NCH = NCHANNELS;
                // if(cType == FrontEndType::CBC3)
                //   NCH = NCHANNELS;
                // else if(cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                //  NCH = NSSACHANNELS;
                // else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
                //  NCH = NMPACHANNELS;

//                float fMean=1.0;
//                        if(cType == FrontEndType::MPA or  cType == FrontEndType::MPA2)
//                     fMean=2.7;
//                        if(cType == FrontEndType::SSA or  cType == FrontEndType::SSA2)
//                     fMean=4.2;
                auto     cOriginalMask = chipDC->getChipOriginalMask();
//                uint32_t nMask         = 0;

                for(uint16_t iChannel = 0; iChannel < chip->size(); ++iChannel)
                {
//                    float cPedestal = chip->getSummary<ThresholdAndNoise, ThresholdAndNoise>().fThreshold;
                    // float cNoise = chip->getSummary<ThresholdAndNoise, ThresholdAndNoise>().fNoise;
                    // LOG(INFO) << BOLDYELLOW << "CHECK "<<iChannel <<", "<<chip->getChannel<ThresholdAndNoise>(iChannel).fNoise<<" "<<fPedeNoiseLimit*fMean<<RESET;
                    // LOG(INFO) << BOLDYELLOW << "CHECK "<<iChannel <<", "<<std::fabs(chip->getChannel<ThresholdAndNoise>(iChannel).fThreshold -cPedestal)<<" "<<fPedeNoiseUntrimmedLimit<<RESET;
                    if(fPedeNoiseMask and (chip->getChannel<ThresholdAndNoise>(iChannel).fNoise > fPedeNoiseLimit))
                    {
                        nMask += 1;
                        LOG(INFO) << BOLDYELLOW << "Masking Channel: " << iChannel << " with a noise of " << chip->getChannel<ThresholdAndNoise>(iChannel).fNoise << ", which is over the limit of "
                                  << fPedeNoiseLimit << RESET;
                        cOriginalMask->disableChannel(iChannel); //Make version of this for masking pixels
                    }
                    if(fPedeNoiseMaskUntrimmed and std::fabs(chip->getChannel<ThresholdAndNoise>(iChannel).fThreshold - cPedestal) > fPedeNoiseUntrimmedLimit)
                    {
                        uint8_t thetrim = fReadoutChipInterface->ReadChipReg(static_cast<ReadoutChip*>(chipDC), "TrimDAC_P" + std::to_string(iChannel + 1));

                        nMask += 1;
                        LOG(INFO) << BOLDYELLOW << "Masking Channel:  " << iChannel << " with a pedestal difference of "
                                  << std::fabs(chip->getChannel<ThresholdAndNoise>(iChannel).fThreshold - cPedestal) << ", which is over the limit of " << fPedeNoiseUntrimmedLimit
                                  << " trimval: " << +thetrim << RESET;
                        cOriginalMask->disableChannel(iChannel); //This is where the channel is disabled
                    }

                    // LOG(INFO) << BOLDYELLOW << "snorp SUMMARY TH "<<cPedestal <<RESET;
                    // LOG(INFO) << BOLDYELLOW << "snorp SUMMARY NOI "<<cNoise <<RESET;
                    // LOG(INFO) << BOLDYELLOW << "fPedeNoiseLimit "<<fPedeNoiseLimit<< " fPedeNoiseMask "<<fPedeNoiseMask <<RESET;
                    // LOG(INFO) << BOLDYELLOW << "Noise "<<iChannel<< ": "<<chip->getChannel<ThresholdAndNoise>(iChannel).fNoise <<RESET;
                    // LOG(INFO) << BOLDYELLOW << "Thresh "<<iChannel<< ": "<<chip->getChannel<ThresholdAndNoise>(iChannel).fThreshold  <<RESET;
                }
                // fReadoutChipInterface->maskChannelGroup(chipDC,cOriginalMask);
                if(nMask > 0) LOG(INFO) << BOLDYELLOW << "PedeNoise masked " << nMask << " channels..." << RESET;
                fReadoutChipInterface->ConfigureChipOriginalMask(chipDC);
            }
 
       }
    }
*/
}

void PixelAlive::writeObjects()
{
#ifdef __USE_ROOT__
    fDQMHistogramPixelAlive.process(); //Comment out plotting for now
#endif
}


//Modify for PixelAlive to run functions (Initialize, measurePixels, Reset)

void PixelAlive::Run()
{
    LOG(INFO) << "Starting Pixel Occupancy Measurement";
    Initialise();
    measurePixels();
    LOG(INFO) << "Done With Occupancy Measurement";
    Reset();
}

//Keep without changing

void PixelAlive::Stoper()
{
    LOG(INFO) << "Stopping Pixel Occupancy Measurement";
    Reset();
    writeObjects();
    dumpConfigFiles(); //Will Dump configuration (pixel masking is only thing changed) into results directory, copy to MPA files if we want to use same settings
    SaveResults();
    closeFileHandler();
    clearDataMembers();
    LOG(INFO) << "Pixel Occupancy Measurement Stopped.";
}

//These need to be here
void PixelAlive::Pause() {}

void PixelAlive::Resume() {}

//Measure Pixel Occupancy
void PixelAlive::measureOccupancy()
{
    for(auto cBoard: *fDetectorContainer)
    {
        if(fWithSSA || fWithMPA)
        {
            // Allow for different SSA and MPA injection amplitudes
            // setSameDacBeBoard(static_cast<BeBoard*>(cBoard), "InjectedCharge", fTestPulseAmplitude);
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        auto cType = cChip->getFrontEndType();

                        if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
			{
                            //fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", fPulseAmplitudePix);
                            fReadoutChipInterface->WriteChipReg(cChip, "Threshold", 100);
                            fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", 100);
			}
                        else
			{
                            //fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", fPulseAmplitude);
                            fReadoutChipInterface->WriteChipReg(cChip, "Threshold", 100);
                            fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", 100);
			}
                    }
                }
            }
        }

        else
            setSameDacBeBoard(static_cast<BeBoard*>(cBoard), "TestPulsePotNodeSel", fPulseAmplitude);
    }



//    if(fUseFixRange) LOG(INFO) << BOLDYELLOW << "Scan should be between " << cLowerLimitTh << " and " << cUpperLimitTh << " DAC units" << RESET;
    auto epsilon     	    = findValueInSettings<double>("PixelEpsilon", 0.1); //Acceptable occupancy > 1 - epsilon

    //DetectorDataContainer* theOccupancyContainer = fRecycleBin.get(&ContainerFactory::copyAndInitStructure<Occupancy>, Occupancy());
    DetectorDataContainer theOccupancyContainer;
    fDetectorDataContainer = &theOccupancyContainer;
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *fDetectorDataContainer);

    bool originalAllChannelFlag = this->fAllChan;
    this->SetTestAllChannels(true);
    
    this->measureData(fNeventsForValidation, fNEventsPerBurst); //Sends pulses and measures occupancy
    this->SetTestAllChannels(originalAllChannelFlag);

#ifdef __USE_ROOT__
    LOG(INFO) << BOLDMAGENTA << "Makeing PixelAlive Plots" << RESET;
    fDQMHistogramPixelAlive.fillValidationPlots(theOccupancyContainer);
#else

//    fDetectorDataContainer                       = theOccupancyContainer;
    std::cout << __PRETTY_FUNCTION__ << "Is stream enabled: " << fDQMStreamerEnabled << std::endl;

    auto theOccupancyStream = prepareHybridContainerStreamer<Occupancy, Occupancy, Occupancy>();
//    auto theOccupancyStream = prepareChannelContainerStreamer<Occupancy>();
    for(auto board: theOccupancyContainer)
    {
        if(fDQMStreamerEnabled) theOccupancyStream->streamAndSendBoard(board, fDQMStreamer);
//        theOccupancyStream->streamAndSendBoard(board, fDQMStreamer);
    }
#endif
    LOG(INFO) << "measuring with " << fEventsPerPoint << " events and " << fNEventsPerBurst << " per burst";

    

    // Retrieve occupancy for strip and pixel chips
//    theOccupancyContainer.normalizeAndAverageContainers(fDetectorContainer, getChannelGroupHandlerContainer(), fEventsPerPoint);

    for(auto cBoard: *fDetectorContainer)
    {
	auto cBoardIdx = cBoard->getId();
        for(auto cOpticalGroup: *cBoard)
        {
            auto cOpticalGroupIdx = cOpticalGroup->getId();
            for(auto cHybrid: *cOpticalGroup)
            {
		auto cHybridIdx = cHybrid->getId();
                for(auto cChip: *cHybrid)
                {	//Add one more loop to loop through each channel within each chip
			   
                    auto cChipIdx       = cChip->getId();
		    auto cType          = cChip->getFrontEndType();
		    uint32_t NCH = NCHANNELS;
		    if(cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                        NCH = NSSACHANNELS;
                    else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
                        NCH = NMPACHANNELS;
		    
                    for(uint32_t iChan = 0; iChan < NCH; iChan++)
		    {
			LOG (INFO) << RED << "Ch " << iChan << RESET ;
//			auto cChannelIdx    = cChannel->getIndex();
			float occupancy = theOccupancyContainer.at(cBoard->getIndex())->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip->getIndex())->getChannel<Occupancy>(iChan).fOccupancy;
			LOG(INFO) << BLUE << "Board(" << cBoardIdx << ") Optical Group(" << cOpticalGroupIdx << ") Hybrid(" << cHybridIdx << ") Chip(" << cChipIdx << ") Channel(" << iChan << ") Occupancy = " << occupancy << RESET;
			if(occupancy > (1.0-epsilon))
			{
			    LOG(INFO) << BLUE << "Occupancy Acceptable" << RESET;
			}
			else
			{
			    LOG(INFO) << BLUE << "Occupancy Unaccptable" << RESET;
			}
		    }

		    
		}
	    }
	}
    }
//#ifdef __USE_ROOT__
//    if(fPlotSCurves) fDQMHistogramPedeNoise.fillSCurvePlots(cStripValue, cPixelValue, *theOccupancyContainer);
//#endif

    // this->HttpServerProcess();
//    LOG(DEBUG) << YELLOW << "Found minimal and maximal occupancy " << cMinBreakCount << " times, SCurves finished! " << RESET;
}


